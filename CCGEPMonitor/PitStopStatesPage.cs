﻿using GTR2SharedMemory.GTR2Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GTR2SharedMemory.GTR2Constants;

namespace CCGEPMonitor
{
  class PitStopStatesPage
  {
    internal class OpponentPitStopInfo
    {
      internal string name = null;
      internal int position = -1;
      internal string vehicleName = null;
      internal string vehicleClass = null;
      internal float fuel = 0.0f;
      internal float fuelPct = 0.0f;
      internal float flWear = 0.0f;
      internal float frWear = 0.0f;
      internal float rlWear = 0.0f;
      internal float rrWear = 0.0f;
      internal GTR2MechanicalFailure mechFailure = GTR2MechanicalFailure.None;
      internal float lapDist = 0.0f;
      internal int lapsCompleted = 0;
      internal GTR2PitState pitState = GTR2PitState.None;
      internal int blueFlag = 0;
      internal int branchID = 0;
      internal float pitLapDist = 0.0f;
      internal GTR2FinishStatus finishStatus = GTR2FinishStatus.None;
      internal string compoundName = "";

      internal bool anyPartDetached;
      internal bool anyWheelDetached;
      internal bool anyWheelFlat;
      internal bool rearWingDetached;

      internal long mID = -1;
    }

    internal class OpponentPitStopTrackedData
    {
      internal GTR2PitState lastPitState = GTR2PitState.None;
      internal int numPitEntries = 0;
      internal float lastPitStopStartET = 0.0f;
      internal float lastPitStopEndET = 0.0f;
      internal float lastPitStopDuration = 0.0f;

      internal float lastPitEntryFrontWear = 0.0f;
      internal float lastPitEntryRearWear = 0.0f;
      internal float lastPitEntryFuel = 0.0f;
      internal bool lastPitEntryAnyPartDetached = false;
      internal bool lastPitEntryAnyWheelDetached = false;
      internal bool lastPitEntryAnyWheelFlat = false;
      internal bool lastPitEntryRearWingDetached = false;
      internal string lastPitEntryCompound;

      internal float lastPitExitFrontWear = 0.0f;
      internal float lastPitExitRearWear = 0.0f;
      internal float lastPitExitFuel = 0.0f;
      internal bool lastPitExitAnyPartDetached = false;
      internal bool lastPitExitAnyWheelDetached = false;
      internal bool lastPitExitAnyWheelFlat = false;
      internal bool lastPitExitRearWingDetached = false;
      internal string lastPitExitCompound;

      internal bool stallInUse = false;
    }
    public static Dictionary<long, OpponentPitStopTrackedData> opponentDataMap = null;

    GTR2GamePhase lastAIStateTrackingGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);

    private float screenYStart = 293.0f;

    internal void RenderPage(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Extended extended, int focusVehId, Graphics g)
    {
      if ((this.lastAIStateTrackingGamePhase == GTR2GamePhase.Garage
          || this.lastAIStateTrackingGamePhase == GTR2GamePhase.SessionOver
          || this.lastAIStateTrackingGamePhase == GTR2GamePhase.SessionStopped
          || (int)this.lastAIStateTrackingGamePhase == 9)  // What is 9? 
        && ((GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Countdown
          || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Formation
          || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GridWalk
          || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GreenFlag))
      {
        PitStopStatesPage.opponentDataMap = null;
      }

      this.lastAIStateTrackingGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0
        || extended.mSessionStarted == 0)
      {
        PitStopStatesPage.opponentDataMap = null;

        return;
      }

      if (PitStopStatesPage.opponentDataMap == null)
        PitStopStatesPage.opponentDataMap = new Dictionary<long, OpponentPitStopTrackedData>();

      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!TransitionTracker.idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = TransitionTracker.idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mPlayerTelemetry;

      var opponentInfos = new List<OpponentPitStopInfo>();
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        var veh = scoring.mVehicles[i];
        var o = new OpponentPitStopInfo();
        o.mID = veh.mID;
        o.name = TransitionTracker.GetStringFromBytes(veh.mDriverName);
        o.position = veh.mPlace;

        o.vehicleName = TransitionTracker.GetStringFromBytes(veh.mVehicleName);
        o.vehicleClass = TransitionTracker.GetStringFromBytes(veh.mVehicleClass);

        var ev = extended.mExtendedVehicleScoring[veh.mID % MAX_MAPPED_IDS];
        o.fuel = ev.mFuel;
        o.fuelPct = ev.mFuelCapacityLiters > 0.0f ? (ev.mFuel / (ev.mFuelCapacityLiters / 100.0f)) : 0.0f;

        const double TIRE_WEAR_MAX = 0.6875;
        const double TIRE_WEAR_RANGE = 1.0 - TIRE_WEAR_MAX;

        o.flWear = (float)((ev.mWheels[(int)GTR2WheelIndex.FrontLeft].mWear - TIRE_WEAR_MAX) / TIRE_WEAR_RANGE) * 100.0f;
        o.frWear = (float)((ev.mWheels[(int)GTR2WheelIndex.FrontRight].mWear - TIRE_WEAR_MAX) / TIRE_WEAR_RANGE) * 100.0f;
        o.rlWear = (float)((ev.mWheels[(int)GTR2WheelIndex.RearLeft].mWear - TIRE_WEAR_MAX) / TIRE_WEAR_RANGE) * 100.0f;
        o.rrWear = (float)((ev.mWheels[(int)GTR2WheelIndex.RearRight].mWear - TIRE_WEAR_MAX) / TIRE_WEAR_RANGE) * 100.0f;

        o.mechFailure = (GTR2MechanicalFailure)ev.mMechanicalFailureID;

        o.lapDist = veh.mLapDist;
        o.lapsCompleted = veh.mTotalLaps;
        o.blueFlag = ev.mBlueFlag;
        o.branchID = ev.mWpBranchID;
        o.pitLapDist = ev.mPitLapDist;

        o.pitState = (GTR2PitState)ev.mPitState;
        o.finishStatus = (GTR2FinishStatus)veh.mFinishStatus;
        o.compoundName = TransitionTracker.GetStringFromBytes(ev.mCurrCompoundName);

        o.anyPartDetached = ev.mDetached != 0;
        o.anyWheelDetached = ev.mWheels[(int)GTR2WheelIndex.FrontLeft].mDetached != 0 || ev.mWheels[(int)GTR2WheelIndex.FrontRight].mDetached != 0
          || ev.mWheels[(int)GTR2WheelIndex.RearLeft].mDetached != 0 || ev.mWheels[(int)GTR2WheelIndex.RearRight].mDetached != 0;
        o.anyWheelFlat = ev.mWheels[(int)GTR2WheelIndex.FrontLeft].mFlat != 0 || ev.mWheels[(int)GTR2WheelIndex.FrontRight].mFlat != 0
          || ev.mWheels[(int)GTR2WheelIndex.RearLeft].mFlat != 0 || ev.mWheels[(int)GTR2WheelIndex.RearRight].mFlat != 0;

        o.rearWingDetached = ev.mHasRearWing != 0 && ev.mRearWingDetached != 0;

        var inPits = veh.mInPits != 0;
        var stallInUse = o.pitState == GTR2PitState.Stopped;
        var speedKMH = (float)Math.Sqrt((veh.mLocalVel.x * veh.mLocalVel.x)
          + (veh.mLocalVel.y * veh.mLocalVel.y)
          + (veh.mLocalVel.z * veh.mLocalVel.z)) * 3.6f;

        // See if any of the opponents uses the stall.
        if (!stallInUse)
        {
          for (int j = 0; j < scoring.mScoringInfo.mNumVehicles && !stallInUse; ++j)
          {
            var veh2 = scoring.mVehicles[j];
            if (veh2.mID == veh.mID)
              continue;

            var ev2 = extended.mExtendedVehicleScoring[veh2.mID % MAX_MAPPED_IDS];
            stallInUse = Math.Abs(veh2.mLapDist - o.pitLapDist) < 10.0 && (GTR2PitState)ev2.mPitState == GTR2PitState.Stopped;
          }
        }

        opponentInfos.Add(o);

        if (!PitStopStatesPage.opponentDataMap.ContainsKey(o.mID))
          PitStopStatesPage.opponentDataMap.Add(o.mID, new OpponentPitStopTrackedData() { lastPitState = o.pitState, stallInUse = stallInUse });
        else
        {
          var otd = PitStopStatesPage.opponentDataMap[o.mID];
          var countPitStopsInThisPhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase != GTR2GamePhase.SessionOver
            && (GTR2GamePhase)scoring.mScoringInfo.mGamePhase != GTR2GamePhase.SessionStopped
            && (GTR2GamePhase)scoring.mScoringInfo.mGamePhase != GTR2GamePhase.Garage;

          if (otd.lastPitState != GTR2PitState.Entering
            && o.pitState == GTR2PitState.Entering
            && countPitStopsInThisPhase)
          {
            //Debug.WriteLine($"PITTING {o.name}   {scoring.mScoringInfo.mLapDist}m   {scoring.mScoringInfo.mCurrentET}s");
            ++otd.numPitEntries;

            otd.lastPitEntryFrontWear = o.flWear;
            otd.lastPitEntryRearWear = o.rlWear;
            otd.lastPitEntryFuel = o.fuel;
            otd.lastPitEntryAnyPartDetached = o.anyPartDetached;
            otd.lastPitEntryAnyWheelDetached = o.anyWheelDetached;
            otd.lastPitEntryAnyWheelFlat = o.anyWheelFlat;
            otd.lastPitEntryRearWingDetached = o.rearWingDetached;
            otd.lastPitEntryCompound = o.compoundName;
          }
          else if (otd.lastPitState == GTR2PitState.Entering && o.pitState == GTR2PitState.Stopped)
          {
            Debug.Assert(inPits);
            otd.lastPitStopStartET = scoring.mScoringInfo.mCurrentET;
            //Debug.WriteLine($"STOPPED {o.name}   {scoring.mScoringInfo.mLapDist}m   {scoring.mScoringInfo.mCurrentET}s");

            /*otd.lastPitEntryFrontWear = o.flWear;
            otd.lastPitEntryRearWear = o.rlWear;
            otd.lastPitEntryFuel = o.fuel;
            otd.lastPitEntryAnyPartDetached = o.anyPartDetached;
            otd.lastPitEntryAnyWheelDetached = o.anyWheelDetached;
            otd.lastPitEntryAnyWheelFlat = o.anyWheelFlat;
            otd.lastPitEntryRearWingDetached = o.rearWingDetached;
            otd.lastPitEntryCompound = o.compoundName;*/
          }
          else if (otd.lastPitState == GTR2PitState.Stopped && o.pitState == GTR2PitState.Exiting)
          {
            //Debug.WriteLine($"EXITING {o.name}   {scoring.mScoringInfo.mLapDist}m   {scoring.mScoringInfo.mCurrentET}s");
            //Debug.Assert(inPits && otd.numPitEntries > 0);
            otd.lastPitStopEndET = scoring.mScoringInfo.mCurrentET;
            otd.lastPitStopDuration = otd.lastPitStopEndET - otd.lastPitStopStartET;

            otd.lastPitExitFrontWear = o.flWear;
            otd.lastPitExitRearWear = o.rlWear;
            otd.lastPitExitFuel = o.fuel;
            otd.lastPitExitAnyPartDetached = o.anyPartDetached;
            otd.lastPitExitAnyWheelDetached = o.anyWheelDetached;
            otd.lastPitExitAnyWheelFlat = o.anyWheelFlat;
            otd.lastPitExitRearWingDetached = o.rearWingDetached;
            otd.lastPitExitCompound = o.compoundName;
          }

          if (o.pitState == GTR2PitState.None
            && inPits)
          {
            if (speedKMH < 0.5f)
            {
              o.pitState = GTR2PitState.Stopped;
              // Debug.WriteLine($"FAKE STOPPED {o.name}   {scoring.mScoringInfo.mCurrentET}");
            }
            if (speedKMH > 5.0f)
            {
              o.pitState = GTR2PitState.Entering;
              //Debug.WriteLine($"FAKE ENTERING {o.name}   {scoring.mScoringInfo.mLapDist}m   {scoring.mScoringInfo.mCurrentET}s");
            }
          }

          otd.lastPitState = o.pitState;
          otd.stallInUse = stallInUse;
        }
      }


      // Order by pos, ascending.
      opponentInfos.Sort((o1, o2) => o1.position.CompareTo(o2.position));

      // TODO_GTR2:
      /*var idsToParticipantIndices = new Dictionary<long, int>();
      for (int i = 0; i < rules.mTrackRules.mNumParticipants; ++i)
      {
        if (!idsToParticipantIndices.ContainsKey(rules.mParticipants[i].mID))
          idsToParticipantIndices.Add(rules.mParticipants[i].mID, i);
      }*/

      if (g != null && MainForm.instance.CurrentPage() == MainForm.Pages.PitStopAnalysis)
      {
        var timingsYStart = this.screenYStart + 3.0f;
        //var font = new Font("Corier New", 10.0f, FontStyle.Bold);
        var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
        g.DrawString($"{"Name (ID)".PadRight(29)}"
          + $"{"Pos".PadLeft(3)}"
          + $"{"Fuel(L/%)".PadLeft(13)}"
          + $"{"Wear(%) FL/FR/RL/RR".PadLeft(21)}"
          + $"{"Mech. Failure".PadLeft(16)}"
          + $"{"Lap Dist.(m)".PadLeft(14)}"
          + $"{"Lap#".PadLeft(5)}"
          + $"{"Pit Entries#/Duration".PadLeft(23)}"
          + $"{"WP Branch#".PadLeft(12)}"
          + $"{"Stall Dist.(m)/In Use".PadLeft(23)}"
          + $"{"Status".PadLeft(10)}"
          + $"{"Compound".PadLeft(10)}"
          + $"{"Lost Part/Wheel/Wh.Flat/RWing".PadLeft(32)}"
          , font, Brushes.Yellow, 3.0f, 20.0f);

        var currYPos = 35.0f;
        foreach (var oi in opponentInfos)
        {
          this.DrawOpponentPitStopInfo(oi, currYPos, focusVehId, g);
          currYPos += 15.0f;
        }

        g.DrawString("Label:", font, Brushes.Yellow, 1765.0f, 20.0f);
        g.DrawString("Sees Blue Flag", font, Brushes.Blue, 1765.0f, 35.0f);
        g.DrawString("Entering Pits", font, Brushes.White, 1765.0f, 50.0f);
        g.DrawString("Pit Requested", font, Brushes.Orange, 1765.0f, 65.0f);
        g.DrawString("Stopped in Pits", font, Brushes.Magenta, 1765.0f, 80.0f);
        g.DrawString("Exiting Pits", font, Brushes.YellowGreen, 1765.0f, 95.0f);

        var sc = extended.mSafetyCar;
        var scSpeedKMH = (float)Math.Sqrt((sc.mLocalVel.x * sc.mLocalVel.x)
          + (sc.mLocalVel.y * sc.mLocalVel.y)
          + (sc.mLocalVel.z * sc.mLocalVel.z)) * 3.6f;

        g.DrawString("SC Lap Distance:", font, Brushes.Yellow, 1765.0f, 125.0f);
        g.DrawString($"{sc.mLapDist.ToString("0.0")}m", font, Brushes.OrangeRed, 1765.0f, 140.0f);
        g.DrawString("SC Speed:", font, Brushes.Yellow, 1765.0f, 155.0f);
        g.DrawString($"{scSpeedKMH.ToString("0.0")}km/h", font, Brushes.OrangeRed, 1765.0f, 170.0f);

        g.DrawString("On-Path Wtnss:", font, Brushes.Yellow, 1765.0f, 200.0f);
        g.DrawString($"{(scoring.mScoringInfo.mOnPathWetness * 100.0f).ToString("0")}%", font, Brushes.OrangeRed, 1765.0f, 215.0f);
        g.DrawString("Off-Path Wtnss:", font, Brushes.Yellow, 1765.0f, 230.0f);
        g.DrawString($"{(scoring.mScoringInfo.mOffPathWetness * 100.0f).ToString("0")}%", font, Brushes.OrangeRed, 1765.0f, 245.0f);
      }
    }

    private void DrawOpponentPitStopInfo(OpponentPitStopInfo oi, float currYPos, int focusVehId, Graphics g)
    {
      var otd = PitStopStatesPage.opponentDataMap[oi.mID];
      var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
      var brush = Brushes.OrangeRed;
      if (oi.blueFlag != 0)
        brush = Brushes.Blue;
      else if (oi.pitState == GTR2PitState.Entering)
        brush = Brushes.White;
      else if (oi.pitState == GTR2PitState.Request)
        brush = Brushes.Orange;
      else if (oi.pitState == GTR2PitState.Stopped)
        brush = Brushes.Magenta;
      else if (oi.pitState == GTR2PitState.Exiting)
        brush = Brushes.YellowGreen;

      g.DrawString((focusVehId != oi.mID ? $"{$"{oi.name} ({oi.mID})".PadRight(29)}" : $"{$">>> {oi.name} ({oi.mID})".PadRight(29)}")
        + $"{oi.position.ToString().PadLeft(3)}"
        + $"{oi.fuel.ToString("0.0")}/{oi.fuelPct.ToString("0.0")}%".PadLeft(13)
        + $"{oi.flWear.ToString("0").PadLeft(6)}"
        + $"{oi.frWear.ToString("0").PadLeft(5)}"
        + $"{oi.rlWear.ToString("0").PadLeft(5)}"
        + $"{oi.rrWear.ToString("0").PadLeft(5)}"
        + $"{TransitionTracker.GetEnumString<GTR2MechanicalFailure>((byte)oi.mechFailure).PadLeft(16)}"
        + $"{oi.lapDist.ToString("0.00").PadLeft(14)}"
        + $"{oi.lapsCompleted.ToString().PadLeft(5)}"
        + $"{otd.numPitEntries.ToString().PadLeft(13)}"
        + $"{TransitionTracker.LapTimeStr(otd.lastPitStopDuration).PadLeft(10)}"
        + $"{oi.branchID.ToString().PadLeft(12)}"
        + $"{oi.pitLapDist.ToString("0.00").PadLeft(15)}"
        + $"{(otd.stallInUse ? "Yes" : "No").PadLeft(8)}"
        + $"{TransitionTracker.GetEnumString<GTR2FinishStatus>((byte)oi.finishStatus).PadLeft(10)}"
        + $"{(oi.compoundName.Substring(0, Math.Min(8, oi.compoundName.Length))).PadLeft(10)}"
        + $"{(oi.anyPartDetached ? "Yes" : "No").PadLeft(14)}{(oi.anyWheelDetached ? "Yes" : "No").PadLeft(6)}{(oi.anyWheelFlat ? "Yes" : "No").PadLeft(6)}{(oi.rearWingDetached ? "Yes" : "No").PadLeft(6)}"
        , font, brush, 3.0f, currYPos);
    }

  }
}
