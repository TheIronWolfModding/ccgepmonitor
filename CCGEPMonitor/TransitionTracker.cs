﻿/*
TransitionTracker class various state transitions in GTR2 state and optionally logs transitions to files.

Author: The Iron Wolf
Website: thecrewchief.org
*/
using GTR2SharedMemory.GTR2Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GTR2SharedMemory.GTR2Constants;

namespace CCGEPMonitor
{
  internal class TransitionTracker
  {
    private static readonly string fileTimesTampString = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
    private static readonly string basePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\logs";
    private static readonly string phaseAndStateTrackingFilePath = $"{basePath}\\{fileTimesTampString}___PhaseAndStateTracking.log";
    private static readonly string damageTrackingFilePath = $"{basePath}\\{fileTimesTampString}___DamageTracking.log";
    private static readonly string rulesTrackingFilePath = $"{basePath}\\{fileTimesTampString}___RulesTracking.log";
    private static readonly string phaseAndStateDeltaTrackingFilePath = $"{basePath}\\{fileTimesTampString}___PhaseAndStateTrackingDelta.log";
    private static readonly string damageTrackingDeltaFilePath = $"{basePath}\\{fileTimesTampString}___DamageTrackingDelta.log";
    private static readonly string rulesTrackingDeltaFilePath = $"{basePath}\\{fileTimesTampString}___RulesTrackingDelta.log";
    private static readonly string timingTrackingFilePath = $"{basePath}\\{fileTimesTampString}___TimingTracking.log";

    internal TransitionTracker()
    {
      if (!Directory.Exists(basePath))
        Directory.CreateDirectory(basePath);
    }

    public static string GetEnumString<T>(sbyte value)
    {
      var enumType = typeof(T);

      var enumValue = (T)Enum.ToObject(enumType, value);
      return Enum.IsDefined(enumType, enumValue) ? enumValue.ToString() : "Unknown";
      //return Enum.IsDefined(enumType, enumValue) ? $"{enumValue.ToString()}({value})" : string.Format("Unknown({0})", value);
    }

    public static string GetEnumString<T>(byte value)
    {
      var enumType = typeof(T);

      var enumValue = (T)Enum.ToObject(enumType, value);
      return Enum.IsDefined(enumType, enumValue) ? enumValue.ToString() : "Unknown";
      //return Enum.IsDefined(enumType, enumValue) ? $"{enumValue.ToString()}({value})" : string.Format("Unknown({0})", value);
    }

    public static string MapToSessionTypeString(ref GTR2Scoring scoring, ref GTR2Extended extended)
    {
      switch (scoring.mScoringInfo.mSession)
      {
        case 0:  // Applies to open practice, private practice, time trial.
                 // This might be problematic - memory may be simply empty.
                 // up to four possible practice sessions (seems 2 in GTR2)
          if (extended.mGameMode == (int)GTR2GameMode.DrivingSchool)
            return $"Driving School";
          //return $"Driving School({scoring.mScoringInfo.mSession})";

          goto case 1;

        case 1:
        case 2:
          var numNonGhostVehicles = 0;
          for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
          {
            var vehicleScoring = scoring.mVehicles[i];
            if (string.Equals(TransitionTracker.GetStringFromBytes(vehicleScoring.mDriverName), "transparent trainer", StringComparison.InvariantCultureIgnoreCase))
              continue;  // Skip trainer.

            ++numNonGhostVehicles;
          }

          return numNonGhostVehicles > 1 // 1 means player only session.
                ? $"Practice"
                : $"Lone Practice";
        //? $"Practice({scoring.mScoringInfo.mSession})"
        //: $"Lone Practice({scoring.mScoringInfo.mSession})";
        // up to four possible qualifying sessions (seems 2 in GTR2)
        case 3:
        case 4:
          return $"Qualification";
        //return $"Qualification({scoring.mScoringInfo.mSession})";
        case 5:
          return $"Practice";  // Warmup really.
                               //return $"Practice({scoring.mScoringInfo.mSession})";  // Warmup really.
        case 6:
          return $"Race";
        //return $"Race({scoring.mScoringInfo.mSession})";
        // up to four possible race sessions
        case 10:
        case 11:
        case 12:
        case 13:
        default:
          return $"Unavailable";
          //return $"Unavailable({scoring.mScoringInfo.mSession})";
      }
    }

    // Telemetry values (separate section)
    internal class PhaseAndState
    {
      internal GTR2GamePhase mGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);
      internal int mSession = -255;
      internal GTR2YellowFlagState mYellowFlagState = (GTR2YellowFlagState)Enum.ToObject(typeof(GTR2YellowFlagState), -255);
      internal int mSector = -255;
      // TODO_GTR2: internal int mCurrentSector = -255;
      internal byte mInRealTimeFC = 255;
      internal byte mInRealTime = 255;
      internal GTR2YellowFlagState mSector1Flag = (GTR2YellowFlagState)Enum.ToObject(typeof(GTR2YellowFlagState), -255);
      internal GTR2YellowFlagState mSector2Flag = (GTR2YellowFlagState)Enum.ToObject(typeof(GTR2YellowFlagState), -255);
      internal GTR2YellowFlagState mSector3Flag = (GTR2YellowFlagState)Enum.ToObject(typeof(GTR2YellowFlagState), -255);
      internal GTR2Control mControl;
      internal byte mInPits = 255;
      internal byte mIsPlayer = 255;
      internal int mPlace = -255;
      internal int mPitState = -255;
      // TODO_GTR2: PlrIinternal GTR2GamePhase mIndividualPhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);
      // TODO_GTR2: internal GTR2PrimaryFlag mFlag = (GTR2PrimaryFlag)Enum.ToObject(typeof(GTR2PrimaryFlag), -255);
      // TODO_GTR2: internal byte mUnderYellow = 255;
      internal int mCountLapFlag = -255;
      // TODO_GTR2: internal byte mInGarageStall = 255;
      internal GTR2FinishStatus mFinishStatus = (GTR2FinishStatus)Enum.ToObject(typeof(GTR2FinishStatus), -255);
      internal int mLapNumber = -255;
      internal short mTotalLaps = -255;
      internal int mMaxLaps = -1;
      internal int mNumVehicles = -1;
      internal byte mScheduledStops = 255;
      // TODO_GTR2: internal byte mHeadlights = 255;
      internal byte mSpeedLimiter = 255;
      // TODO_GTR2: internal byte mFrontTireCompoundIndex = 255;
      internal int mCurrTireCompoundIndex = -255;
      // TODO_GTR2: internal string mFrontTireCompoundName = "Unknown";
      internal string mCurrTireCompoundName = "Unknown";
      // TODO_GTR2: internal byte mRearFlapActivated = 255;
      internal byte mCurrDRSSystemState = 255;
      // TODO_GTR2: internal GTR2IgnitionStarterStatus mIgnitionStarter = (GTR2IgnitionStarterStatus)Enum.ToObject(typeof(GTR2IgnitionStarterStatus), -255);
      internal byte mSpeedLimiterAvailable = 255;
      internal byte mAntiStallActivated = 255;
      internal byte mStartLight = 255;
      internal byte mNumRedLights = 255;
      internal short mNumPitstops = -255;
      internal short mNumPenalties = -255;
      internal int mLapsBehindNext = -1;
      internal int mLapsBehindLeader = -1;
      // TODO_GTR2: internal byte mPlayerHeadlights = 255;
      // TODO_GTR2: internal byte mServerScored = 255;
      // TODO_GTR2: internal int mQualification = -1;
    }

    internal PhaseAndState prevPhaseAndSate = new PhaseAndState();
    internal StringBuilder sbPhaseChanged = new StringBuilder();
    internal StringBuilder sbPhaseLabel = new StringBuilder();
    internal StringBuilder sbPhaseValues = new StringBuilder();
    internal StringBuilder sbPhaseChangedCol2 = new StringBuilder();
    internal StringBuilder sbPhaseLabelCol2 = new StringBuilder();
    internal StringBuilder sbPhaseValuesCol2 = new StringBuilder();

    GTR2GamePhase lastDamageTrackingGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);
    GTR2GamePhase lastPhaseTrackingGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);
    GTR2GamePhase lastTimingTrackingGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);
    GTR2GamePhase lastRulesTrackingGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);

    private float screenYStart = 293.0f;

    internal void TrackPhase(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Extended extended, Graphics g, bool logToFile)
    {
      if (logToFile)
      {
        if ((this.lastPhaseTrackingGamePhase == GTR2GamePhase.Garage
              || this.lastPhaseTrackingGamePhase == GTR2GamePhase.SessionOver
              || this.lastPhaseTrackingGamePhase == GTR2GamePhase.SessionStopped
              || (int)this.lastPhaseTrackingGamePhase == 9)  // What is 9? 
            && ((GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Countdown
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Formation
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GridWalk
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GreenFlag))
        {
          var lines = new List<string>();
          lines.Add("\n");
          lines.Add("************************************************************************************");
          lines.Add("* NEW SESSION **********************************************************************");
          lines.Add("************************************************************************************");
          File.AppendAllLines(phaseAndStateTrackingFilePath, lines);
          File.AppendAllLines(phaseAndStateDeltaTrackingFilePath, lines);
        }
      }

      this.lastPhaseTrackingGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0)
        return;

      // Build map of mID -> telemetry.mVehicles[i]. 
      // They are typically matching values, however, we need to handle online cases and dropped vehicles (mID can be reused).
      TransitionTracker.idsToTelIndices.Clear();
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        if (!TransitionTracker.idsToTelIndices.ContainsKey(scoring.mVehicles[i].mID))
          TransitionTracker.idsToTelIndices.Add(scoring.mVehicles[i].mID, i);
      }

      TransitionTracker.playerVeh = MainForm.GetPlayerScoring(ref scoring);
      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!TransitionTracker.idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = TransitionTracker.idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mPlayerTelemetry;

      var ps = new PhaseAndState();

      ps.mGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;
      ps.mSession = scoring.mScoringInfo.mSession;
      ps.mYellowFlagState = (GTR2YellowFlagState)scoring.mScoringInfo.mYellowFlagState;
      ps.mSector = TransitionTracker.playerVeh.mSector == 0 ? 3 : TransitionTracker.playerVeh.mSector;
      // TODO_GTR2: ps.mCurrentSector = TransitionTracker.playerVehT.mCurrentSector;
      ps.mInRealTime = scoring.mScoringInfo.mInRealtime;
      ps.mInRealTimeFC = extended.mInRealtimeFC;
      ps.mSector1Flag = (GTR2YellowFlagState)scoring.mScoringInfo.mSectorFlag[0];
      ps.mSector2Flag = (GTR2YellowFlagState)scoring.mScoringInfo.mSectorFlag[1];
      ps.mSector3Flag = (GTR2YellowFlagState)scoring.mScoringInfo.mSectorFlag[2];
      ps.mControl = (GTR2Control)TransitionTracker.playerVeh.mControl;
      ps.mInPits = TransitionTracker.playerVeh.mInPits;
      ps.mIsPlayer = TransitionTracker.playerVeh.mIsPlayer;
      ps.mPlace = TransitionTracker.playerVeh.mPlace;
      ps.mPitState = extended.mExtendedVehicleScoring[scoringPlrId].mPitState;
      // TODO_GTR2: ps.mIndividualPhase = (GTR2GamePhase)TransitionTracker.playerVeh.mIndividualPhase;
      // TODO_GTR2: ps.mFlag = (GTR2PrimaryFlag)TransitionTracker.playerVeh.mFlag;
      // TODO_GTR2: ps.mUnderYellow = TransitionTracker.playerVeh.mUnderYellow;
      ps.mCountLapFlag = extended.mExtendedVehicleScoring[scoringPlrId].mCountLapFlag;
      // TODO_GTR2: ps.mInGarageStall = TransitionTracker.playerVeh.mInGarageStall;
      ps.mFinishStatus = (GTR2FinishStatus)TransitionTracker.playerVeh.mFinishStatus;
      ps.mLapNumber = playerVehTelemetry.mLapNumber;
      ps.mTotalLaps = TransitionTracker.playerVeh.mTotalLaps;
      ps.mMaxLaps = scoring.mScoringInfo.mMaxLaps;
      ps.mNumVehicles = scoring.mScoringInfo.mNumVehicles;
      ps.mScheduledStops = playerVehTelemetry.mScheduledStops;
      // TODO_GTR2: ps.mHeadlights = TransitionTracker.playerVeh.mHeadlights;
      ps.mSpeedLimiter = extended.mExtendedVehicleScoring[scoringPlrId].mSpeedLimiter;
      // TODO_GTR2: ps.mFrontTireCompoundIndex = playerVehTelemetry.mFrontTireCompoundIndex;
      ps.mCurrTireCompoundIndex = extended.mExtendedVehicleScoring[scoringPlrId].mTireCompoundIndex;
      // TODO_GTR2: ps.mFrontTireCompoundName = TransitionTracker.GetStringFromBytes(playerVehTelemetry.mFrontTireCompoundName);
      ps.mCurrTireCompoundName = TransitionTracker.GetStringFromBytes(extended.mExtendedVehicleScoring[scoringPlrId].mCurrCompoundName);
      // TODO_GTR2: ps.mFrontFlapActivated = playerVehTelemetry.mFrontFlapActivated;
      // TODO_GTR2: ps.mRearFlapActivated = playerVehTelemetry.mRearFlapActivated;
      ps.mCurrDRSSystemState = extended.mCurrDRSSystemState;
      // TODO_GTR2: ps.mIgnitionStarter = (GTR2IgnitionStarterStatus)playerVehTelemetry.mIgnitionStarter;
      ps.mSpeedLimiterAvailable = extended.mExtendedVehicleScoring[scoringPlrId].mSpeedLimiter;
      ps.mAntiStallActivated = extended.mAntistallActive;
      ps.mStartLight = scoring.mScoringInfo.mStartLight;
      ps.mNumRedLights = scoring.mScoringInfo.mNumRedLights;
      ps.mNumPitstops = TransitionTracker.playerVeh.mNumPitstops;
      ps.mNumPenalties = TransitionTracker.playerVeh.mNumPenalties;
      ps.mLapsBehindNext = TransitionTracker.playerVeh.mLapsBehindNext;
      ps.mLapsBehindLeader = TransitionTracker.playerVeh.mLapsBehindLeader;
      // TODO_GTR2: ps.mPlayerHeadlights = TransitionTracker.playerVeh.mHeadlights;
      // TODO_GTR2: ps.mServerScored = TransitionTracker.playerVeh.mServerScored;
      // TODO_GTR2: ps.mQualification = TransitionTracker.playerVeh.mQualification;

      // Only refresh UI if there's change.
      if (this.prevPhaseAndSate.mGamePhase != ps.mGamePhase
        || this.prevPhaseAndSate.mSession != ps.mSession
        || this.prevPhaseAndSate.mYellowFlagState != ps.mYellowFlagState
        || this.prevPhaseAndSate.mSector != ps.mSector
        // TODO_GTR2: || this.prevPhaseAndSate.mCurrentSector != ps.mCurrentSector
        || this.prevPhaseAndSate.mInRealTimeFC != ps.mInRealTimeFC
        || this.prevPhaseAndSate.mInRealTime != ps.mInRealTime
        || this.prevPhaseAndSate.mSector1Flag != ps.mSector1Flag
        || this.prevPhaseAndSate.mSector2Flag != ps.mSector2Flag
        || this.prevPhaseAndSate.mSector3Flag != ps.mSector3Flag
        || this.prevPhaseAndSate.mControl != ps.mControl
        || this.prevPhaseAndSate.mInPits != ps.mInPits
        || this.prevPhaseAndSate.mIsPlayer != ps.mIsPlayer
        || this.prevPhaseAndSate.mPlace != ps.mPlace
        || this.prevPhaseAndSate.mPitState != ps.mPitState
        // TODO_GTR2: || this.prevPhaseAndSate.mIndividualPhase != ps.mIndividualPhase
        // TODO_GTR2: || this.prevPhaseAndSate.mFlag != ps.mFlag
        // TODO_GTR2: || this.prevPhaseAndSate.mUnderYellow != ps.mUnderYellow
        || this.prevPhaseAndSate.mCountLapFlag != ps.mCountLapFlag
        // TODO_GTR2: || this.prevPhaseAndSate.mInGarageStall != ps.mInGarageStall
        || this.prevPhaseAndSate.mFinishStatus != ps.mFinishStatus
        || this.prevPhaseAndSate.mLapNumber != ps.mLapNumber
        || this.prevPhaseAndSate.mTotalLaps != TransitionTracker.playerVeh.mTotalLaps
        || this.prevPhaseAndSate.mMaxLaps != ps.mMaxLaps
        || this.prevPhaseAndSate.mNumVehicles != ps.mNumVehicles
        || this.prevPhaseAndSate.mScheduledStops != ps.mScheduledStops
        // TODO_GTR2: || this.prevPhaseAndSate.mHeadlights != ps.mHeadlights
        || this.prevPhaseAndSate.mSpeedLimiter != ps.mSpeedLimiter
        // TODO_GTR2: || this.prevPhaseAndSate.mFrontTireCompoundIndex != ps.mFrontTireCompoundIndex
        || this.prevPhaseAndSate.mCurrTireCompoundIndex != ps.mCurrTireCompoundIndex
        // TODO_GTR2: || this.prevPhaseAndSate.mFrontTireCompoundName != ps.mFrontTireCompoundName
        || this.prevPhaseAndSate.mCurrTireCompoundName != ps.mCurrTireCompoundName
        // TODO_GTR2: || this.prevPhaseAndSate.mFrontFlapActivated != ps.mFrontFlapActivated
        // TODO_GTR2: || this.prevPhaseAndSate.mRearFlapActivated != ps.mRearFlapActivated
        || this.prevPhaseAndSate.mCurrDRSSystemState != ps.mCurrDRSSystemState
        // TODO_GTR2: || this.prevPhaseAndSate.mIgnitionStarter != ps.mIgnitionStarter
        || this.prevPhaseAndSate.mSpeedLimiterAvailable != ps.mSpeedLimiterAvailable
        || this.prevPhaseAndSate.mAntiStallActivated != ps.mAntiStallActivated
        || this.prevPhaseAndSate.mStartLight != ps.mStartLight
        || this.prevPhaseAndSate.mNumRedLights != ps.mNumRedLights
        || this.prevPhaseAndSate.mNumPitstops != ps.mNumPitstops
        || this.prevPhaseAndSate.mNumPenalties != ps.mNumPenalties
        || this.prevPhaseAndSate.mLapsBehindNext != ps.mLapsBehindNext
        || this.prevPhaseAndSate.mLapsBehindLeader != ps.mLapsBehindLeader)
      // TODO_GTR2: || this.prevPhaseAndSate.mPlayerHeadlights != ps.mHeadlights
      // TODO_GTR2: || this.prevPhaseAndSate.mServerScored != ps.mServerScored
      // TODO_GTR2: || this.prevPhaseAndSate.mQualification != ps.mQualification)
      {
        this.sbPhaseChanged = new StringBuilder();
        sbPhaseChanged.Append((this.prevPhaseAndSate.mGamePhase != ps.mGamePhase ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSession != ps.mSession ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mYellowFlagState != ps.mYellowFlagState ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSector != ps.mSector ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mCurrentSector != ps.mCurrentSector ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mInRealTimeFC != ps.mInRealTimeFC ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mInRealTime != ps.mInRealTime ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSector1Flag != ps.mSector1Flag ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSector2Flag != ps.mSector2Flag ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSector3Flag != ps.mSector3Flag ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mControl != ps.mControl ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mInPits != ps.mInPits ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mIsPlayer != ps.mIsPlayer ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mPlace != ps.mPlace ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mPitState != ps.mPitState ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mIndividualPhase != ps.mIndividualPhase ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mFlag != ps.mFlag ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mUnderYellow != ps.mUnderYellow ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mCountLapFlag != ps.mCountLapFlag ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mInGarageStall != ps.mInGarageStall ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mFinishStatus != ps.mFinishStatus ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mLapNumber != ps.mLapNumber ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mTotalLaps != ps.mTotalLaps ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mMaxLaps != ps.mMaxLaps ? "***\n" : "\n"));

        this.sbPhaseChangedCol2 = new StringBuilder();
        sbPhaseChangedCol2.Append((this.prevPhaseAndSate.mNumVehicles != ps.mNumVehicles ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mScheduledStops != ps.mScheduledStops ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mHeadlights != ps.mHeadlights ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSpeedLimiter != ps.mSpeedLimiter ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mFrontTireCompoundIndex != ps.mFrontTireCompoundIndex ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mCurrTireCompoundIndex != ps.mCurrTireCompoundIndex ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mFrontTireCompoundName != ps.mFrontTireCompoundName ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mCurrTireCompoundName != ps.mCurrTireCompoundName ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mFrontFlapActivated != ps.mFrontFlapActivated ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mRearFlapActivated != ps.mRearFlapActivated ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mRearFlapLegalStatus != ps.mRearFlapLegalStatus ? "***\n" : "\n")
          // TODO_GTR2: + (this.prevPhaseAndSate.mIgnitionStarter != ps.mIgnitionStarter ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mSpeedLimiterAvailable != ps.mSpeedLimiterAvailable ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mAntiStallActivated != ps.mAntiStallActivated ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mStartLight != ps.mStartLight ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mNumRedLights != ps.mNumRedLights ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mNumPitstops != ps.mNumPitstops ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mNumPenalties != ps.mNumPenalties ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mLapsBehindNext != ps.mLapsBehindNext ? "***\n" : "\n")
          + (this.prevPhaseAndSate.mLapsBehindLeader != ps.mLapsBehindLeader ? "***\n" : "\n"));
        // TODO_GTR2: + (this.prevPhaseAndSate.mPlayerHeadlights != ps.mPlayerHeadlights ? "***\n" : "\n")
        // TODO_GTR2: + (this.prevPhaseAndSate.mServerScored != ps.mServerScored ? "***\n" : "\n")
        // TODO_GTR2: + (this.prevPhaseAndSate.mQualification != ps.mQualification ? "***\n" : "\n"));

        // Save current phase and state.
        this.prevPhaseAndSate = ps;

        this.sbPhaseLabel = new StringBuilder();
        sbPhaseLabel.Append("mGamePhase:\n"
          + "mSession:\n"
          + "mYellowFlagState:\n"
          + "mSector:\n"
          + "mInRealtimeFC:\n"
          + "mInRealtimeSU:\n"
          + "mSectorFlag[0]:\n"
          + "mSectorFlag[1]:\n"
          + "mSectorFlag[2]:\n"
          + "mControl:\n"
          + "mInPits:\n"
          + "mIsPlayer:\n"
          + "mPlace:\n"
          + "mCountLapFlag\n"
          + "mFinishStatus:\n"
          + "mLapNumber:\n"
          + "mTotalLaps:\n"
          + "mMaxLaps:\n");

        this.sbPhaseLabelCol2 = new StringBuilder();
        sbPhaseLabelCol2.Append("mNumVehicles:\n"
          + "mScheduledStops:\n"
          + "mSpeedLimiter:\n"
          + "mTireCompoundIndex:\n"
          + "mCurrTireCompoundName:\n"
          + "mCurrDRSSystemState:\n"
          + "mSpeedLimiterAvailable:\n"
          + "mAntiStallActivated:\n"
          + "mStartLight:\n"
          + "mNumRedLights:\n"
          + "mNumPitstops:\n"
          + "mNumPenalties:\n"
          + "mLapsBehindNext:\n"
          + "mLapsBehindLeader:\n"
        );

        this.sbPhaseValues = new StringBuilder();
        sbPhaseValues.Append(
          $"{GetEnumString<GTR2GamePhase>(scoring.mScoringInfo.mGamePhase)}\n"
          + $"{TransitionTracker.MapToSessionTypeString(ref scoring, ref extended)}\n"
          + $"{GetEnumString<GTR2YellowFlagState>(scoring.mScoringInfo.mYellowFlagState)}\n"
          + $"{ps.mSector}\n"
          + (ps.mInRealTimeFC == 0 ? $"false({ps.mInRealTimeFC})" : $"true({ps.mInRealTimeFC})") + "\n"
          + (ps.mInRealTime == 0 ? $"false({ps.mInRealTime})" : $"true({ps.mInRealTime})") + "\n"
          + $"{GetEnumString<GTR2YellowFlagState>(scoring.mScoringInfo.mSectorFlag[0])}\n"
          + $"{GetEnumString<GTR2YellowFlagState>(scoring.mScoringInfo.mSectorFlag[1])}\n"
          + $"{GetEnumString<GTR2YellowFlagState>(scoring.mScoringInfo.mSectorFlag[2])}\n"
          + $"{GetEnumString<GTR2Control>(TransitionTracker.playerVeh.mControl)}\n"
          + (ps.mInPits == 0 ? $"false({ps.mInPits})" : $"true({ps.mInPits})") + "\n"
          + (ps.mIsPlayer == 0 ? $"false({ps.mIsPlayer})" : $"true({ps.mIsPlayer})") + "\n"
          + $"{ps.mPlace}\n"
          + $"{ps.mCountLapFlag}\n"
          + $"{GetEnumString<GTR2FinishStatus>(TransitionTracker.playerVeh.mFinishStatus)}\n"
          + $"{ps.mLapNumber}\n"
          + $"{ps.mTotalLaps}\n"
          + $"{ps.mMaxLaps}\n");

        this.sbPhaseValuesCol2 = new StringBuilder();
        sbPhaseValuesCol2.Append($"{ps.mNumVehicles}\n"
          + (ps.mScheduledStops == 0 ? $"false({ps.mScheduledStops})" : $"true({ps.mScheduledStops})") + "\n"
          + (ps.mSpeedLimiter == 0 ? $"false({ps.mSpeedLimiter})" : $"true({ps.mSpeedLimiter})") + "\n"
          + (ps.mCurrTireCompoundIndex == 0 ? $"false({ps.mCurrTireCompoundIndex})" : $"true({ps.mCurrTireCompoundIndex})") + "\n"
          + $"{ps.mCurrTireCompoundName}\n"
          + $"{ps.mCurrDRSSystemState}\n"
          + (ps.mSpeedLimiterAvailable == 0 ? $"false({ps.mSpeedLimiterAvailable})" : $"true({ps.mSpeedLimiterAvailable})") + "\n"
          + (ps.mAntiStallActivated == 0 ? $"false({ps.mAntiStallActivated})" : $"true({ps.mAntiStallActivated})") + "\n"
          + $"{ps.mStartLight}\n"
          + $"{ps.mNumRedLights}\n"
          + $"{ps.mNumPitstops}\n"
          + $"{ps.mNumPenalties}\n"
          + $"{ps.mLapsBehindNext}\n"
          + $"{ps.mLapsBehindLeader}\n");

        if (logToFile)
        {
          var changed = this.sbPhaseChanged.ToString().Split('\n');
          var labels = this.sbPhaseLabel.ToString().Split('\n');
          var values = this.sbPhaseValues.ToString().Split('\n');

          var changedCol2 = this.sbPhaseChangedCol2.ToString().Split('\n');
          var labelsCol2 = this.sbPhaseLabelCol2.ToString().Split('\n');
          var valuesCol2 = this.sbPhaseValuesCol2.ToString().Split('\n');

          var list = new List<string>(changed);
          list.AddRange(changedCol2);
          changed = list.ToArray();

          list = new List<string>(labels);
          list.AddRange(labelsCol2);
          labels = list.ToArray();

          list = new List<string>(values);
          list.AddRange(valuesCol2);
          values = list.ToArray();

          Debug.Assert(changed.Length == labels.Length && values.Length == labels.Length);

          var lines = new List<string>();
          var updateTime = DateTime.Now.ToString();

          lines.Add($"\n{updateTime}");
          for (int i = 0; i < changed.Length; ++i)
            lines.Add($"{changed[i]}{labels[i]}{values[i]}");

          File.AppendAllLines(phaseAndStateTrackingFilePath, lines);

          lines.Clear();

          lines.Add($"\n{updateTime}");
          for (int i = 0; i < changed.Length; ++i)
          {
            if (changed[i].StartsWith("***"))
              lines.Add($"{changed[i]}{labels[i]}{values[i]}");
          }

          File.AppendAllLines(phaseAndStateDeltaTrackingFilePath, lines);
        }
      }

      if (g != null && MainForm.instance.CurrentPage() == MainForm.Pages.PlayerVehDamageAndTimings)
      {
        g.DrawString(this.sbPhaseChanged.ToString(), SystemFonts.DefaultFont, Brushes.Orange, 3.0f, this.screenYStart + 3.0f);
        g.DrawString(this.sbPhaseLabel.ToString(), SystemFonts.DefaultFont, Brushes.Yellow, 30.0f, this.screenYStart);
        g.DrawString(this.sbPhaseValues.ToString(), SystemFonts.DefaultFont, Brushes.OrangeRed, 160.0f, this.screenYStart);

        g.DrawString(this.sbPhaseChangedCol2.ToString(), SystemFonts.DefaultFont, Brushes.Orange, 253.0f, this.screenYStart + 3.0f);
        g.DrawString(this.sbPhaseLabelCol2.ToString(), SystemFonts.DefaultFont, Brushes.Yellow, 280.0f, this.screenYStart);
        g.DrawString(this.sbPhaseValuesCol2.ToString(), SystemFonts.DefaultFont, Brushes.OrangeRed, 440.0f, this.screenYStart);
      }
    }

    public static string GetStringFromBytes(byte[] bytes)
    {
      if (bytes == null)
        return "";

      var nullIdx = Array.IndexOf(bytes, (byte)0);

      return nullIdx >= 0
        ? Encoding.Default.GetString(bytes, 0, nullIdx)
        : Encoding.Default.GetString(bytes);
    }

    internal class DamageInfo
    {
      internal byte[] mDentSeverity = new byte[8];         // dent severity at 8 locations around the car (0=none, 1=some, 2=more)
      internal double mLastImpactMagnitude = -1.0;   // magnitude of last impact
      internal double mAccumulatedImpactMagnitude = -1.0;   // magnitude of last impact
      internal double mMaxImpactMagnitude = -1.0;   // magnitude of last impact
      internal GTR2Vec3 mLastImpactPos;        // location of last impact
      internal double mLastImpactET = -1.0;          // time of last impact
      internal byte mOverheating = 255;            // whether overheating icon is shown
      internal byte mDetached = 255;               // whether any parts (besides wheels) have been detached
      //internal byte mHeadlights = 255;             // whether headlights are on

      internal byte mFrontLeftFlat = 255;                    // whether tire is flat
      internal byte mFrontLeftDetached = 255;                // whether wheel is detached
      internal byte mFrontRightFlat = 255;                    // whether tire is flat
      internal byte mFrontRightDetached = 255;                // whether wheel is detached

      internal byte mRearLeftFlat = 255;                    // whether tire is flat
      internal byte mRearLeftDetached = 255;                // whether wheel is detached
      internal byte mRearRightFlat = 255;                    // whether tire is flat
      internal byte mRearRightDetached = 255;                // whether wheel is detached
    }

    internal DamageInfo prevDamageInfo = new DamageInfo();
    internal StringBuilder sbDamageChanged = new StringBuilder();
    internal StringBuilder sbDamageLabel = new StringBuilder();
    internal StringBuilder sbDamageValues = new StringBuilder();

    internal void TrackDamage(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Extended extended, Graphics g, bool logToFile)
    {
      if (logToFile)
      {
        if ((this.lastDamageTrackingGamePhase == GTR2GamePhase.Garage
              || this.lastDamageTrackingGamePhase == GTR2GamePhase.SessionOver
              || this.lastDamageTrackingGamePhase == GTR2GamePhase.SessionStopped
              || (int)this.lastDamageTrackingGamePhase == 9)  // What is 9? 
            && ((GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Countdown
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Formation
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GridWalk
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GreenFlag))
        {
          var lines = new List<string>();
          lines.Add("\n");
          lines.Add("************************************************************************************");
          lines.Add("* NEW SESSION **********************************************************************");
          lines.Add("************************************************************************************");
          File.AppendAllLines(damageTrackingFilePath, lines);
          File.AppendAllLines(damageTrackingDeltaFilePath, lines);
        }
      }

      this.lastDamageTrackingGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0)
        return;

      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = TransitionTracker.idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mPlayerTelemetry;

      var di = new DamageInfo();
      di.mDentSeverity = playerVehTelemetry.mDentSeverity;
      di.mLastImpactMagnitude = playerVehTelemetry.mLastImpactMagnitude;
      di.mAccumulatedImpactMagnitude = extended.mTrackedDamages[resolvedIdx % MAX_MAPPED_IDS].mAccumulatedImpactMagnitude;
      di.mMaxImpactMagnitude = extended.mTrackedDamages[resolvedIdx % MAX_MAPPED_IDS].mMaxImpactMagnitude;
      di.mLastImpactPos = playerVehTelemetry.mLastImpactPos;
      di.mLastImpactET = playerVehTelemetry.mLastImpactET;
      di.mOverheating = playerVehTelemetry.mOverheating;
      di.mDetached = playerVehTelemetry.mDetached;
      di.mFrontLeftFlat = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.FrontLeft].mFlat;
      di.mFrontLeftDetached = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.FrontLeft].mDetached;
      di.mFrontRightFlat = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.FrontRight].mFlat;
      di.mFrontRightDetached = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.FrontRight].mDetached;
      di.mRearLeftFlat = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.RearLeft].mFlat;
      di.mRearLeftDetached = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.RearLeft].mDetached;
      di.mRearRightFlat = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.RearRight].mFlat;
      di.mRearRightDetached = playerVehTelemetry.mWheel[(int)GTR2WheelIndex.RearRight].mDetached;

      bool dentSevChanged = false;
      for (int i = 0; i < 8; ++i)
      {
        if (this.prevDamageInfo.mDentSeverity[i] != di.mDentSeverity[i])
        {
          dentSevChanged = true;
          break;
        }
      }

      bool lastImpactPosChanged = di.mLastImpactPos.x != this.prevDamageInfo.mLastImpactPos.x
        || di.mLastImpactPos.y != this.prevDamageInfo.mLastImpactPos.y
        || di.mLastImpactPos.z != this.prevDamageInfo.mLastImpactPos.z;

      // Only refresh UI if there's change.
      if (dentSevChanged
        || di.mLastImpactMagnitude != this.prevDamageInfo.mLastImpactMagnitude
        || di.mAccumulatedImpactMagnitude != this.prevDamageInfo.mAccumulatedImpactMagnitude
        || di.mMaxImpactMagnitude != this.prevDamageInfo.mMaxImpactMagnitude
        || lastImpactPosChanged
        || di.mLastImpactET != this.prevDamageInfo.mLastImpactET
        || di.mOverheating != this.prevDamageInfo.mOverheating
        || di.mDetached != this.prevDamageInfo.mDetached
        || di.mFrontLeftFlat != this.prevDamageInfo.mFrontLeftFlat
        || di.mFrontRightFlat != this.prevDamageInfo.mFrontRightFlat
        || di.mRearLeftFlat != this.prevDamageInfo.mRearLeftFlat
        || di.mRearRightFlat != this.prevDamageInfo.mRearRightFlat
        || di.mFrontLeftDetached != this.prevDamageInfo.mFrontLeftDetached
        || di.mFrontRightDetached != this.prevDamageInfo.mFrontRightDetached
        || di.mRearLeftDetached != this.prevDamageInfo.mRearLeftDetached
        || di.mRearRightDetached != this.prevDamageInfo.mRearRightDetached)
      {
        this.sbDamageChanged = new StringBuilder();
        sbDamageChanged.Append((dentSevChanged ? "***\n" : "\n")
          + (di.mLastImpactMagnitude != this.prevDamageInfo.mLastImpactMagnitude ? "***\n" : "\n")
          + (di.mAccumulatedImpactMagnitude != this.prevDamageInfo.mAccumulatedImpactMagnitude ? "***\n" : "\n")
          + (di.mMaxImpactMagnitude != this.prevDamageInfo.mMaxImpactMagnitude ? "***\n" : "\n")
          + (lastImpactPosChanged ? "***\n" : "\n")
          + (di.mLastImpactET != this.prevDamageInfo.mLastImpactET ? "***\n" : "\n")
          + (di.mOverheating != this.prevDamageInfo.mOverheating ? "***\n" : "\n")
          + (di.mDetached != this.prevDamageInfo.mDetached ? "***\n" : "\n")
          + ((di.mFrontLeftFlat != this.prevDamageInfo.mFrontLeftFlat
              || di.mFrontRightFlat != this.prevDamageInfo.mFrontRightFlat
              || di.mFrontLeftDetached != this.prevDamageInfo.mFrontLeftDetached
              || di.mFrontRightDetached != this.prevDamageInfo.mFrontRightDetached) ? "***\n" : "\n")
          + ((di.mRearLeftFlat != this.prevDamageInfo.mRearLeftFlat
              || di.mRearRightFlat != this.prevDamageInfo.mRearRightFlat
              || di.mRearLeftDetached != this.prevDamageInfo.mRearLeftDetached
              || di.mRearRightDetached != this.prevDamageInfo.mRearRightDetached) ? "***\n" : "\n"));

        // Save current damage info.
        this.prevDamageInfo = di;

        this.sbDamageLabel = new StringBuilder();
        sbDamageLabel.Append(
          "mDentSeverity:\n"
          + "mLastImpactMagnitude:\n"
          + "mAccumulatedImpactMagnitude:\n"
          + "mMaxImpactMagnitude:\n"
          + "mLastImpactPos:\n"
          + "mLastImpactET:\n"
          + "mOverheating:\n"
          + "mDetached:\n"
          + "Front Wheels:\n"
          + "Rear Wheels:\n");

        this.sbDamageValues = new StringBuilder();
        sbDamageValues.Append(
          $"{di.mDentSeverity[0]},{di.mDentSeverity[1]},{di.mDentSeverity[2]},{di.mDentSeverity[3]},{di.mDentSeverity[4]},{di.mDentSeverity[5]},{di.mDentSeverity[6]},{di.mDentSeverity[7]}\n"
          + $"{di.mLastImpactMagnitude:N1}\n"
          + $"{di.mAccumulatedImpactMagnitude:N1}\n"
          + $"{di.mMaxImpactMagnitude:N1}\n"
          + $"x={di.mLastImpactPos.x:N4} y={di.mLastImpactPos.y:N4} z={di.mLastImpactPos.z:N4}\n"
          + $"{di.mLastImpactET}\n"
          + $"{di.mOverheating}\n"
          + $"{di.mDetached}\n"
          + $"Left Flat:{di.mFrontLeftFlat}    Left Detached:{di.mFrontLeftDetached}        Right Flat:{di.mFrontRightFlat}    Right Detached:{di.mFrontRightDetached}\n"
          + $"Left Flat:{di.mRearLeftFlat}    Left Detached:{di.mRearLeftDetached}        Right Flat:{di.mRearRightFlat}    Right Detached:{di.mRearRightDetached}\n"
          );

        if (logToFile)
        {
          var changed = this.sbDamageChanged.ToString().Split('\n');
          var labels = this.sbDamageLabel.ToString().Split('\n');
          var values = this.sbDamageValues.ToString().Split('\n');
          Debug.Assert(changed.Length == labels.Length && values.Length == labels.Length);

          var lines = new List<string>();

          var updateTime = DateTime.Now.ToString();
          lines.Add($"\n{updateTime}");
          for (int i = 0; i < changed.Length; ++i)
            lines.Add($"{changed[i]}{labels[i]}{values[i]}");

          File.AppendAllLines(damageTrackingFilePath, lines);

          lines.Clear();
          lines.Add($"\n{updateTime}");
          for (int i = 0; i < changed.Length; ++i)
          {
            if (changed[i].StartsWith("***"))
              lines.Add($"{changed[i]}{labels[i]}{values[i]}");
          }

          File.AppendAllLines(damageTrackingDeltaFilePath, lines);
        }
      }

      if (g != null && MainForm.instance.CurrentPage() == MainForm.Pages.PlayerVehDamageAndTimings)
      {
        var dmgYStart = this.screenYStart + 310.0f;
        g.DrawString(this.sbDamageChanged.ToString(), SystemFonts.DefaultFont, Brushes.Orange, 3.0f, dmgYStart + 3.0f);
        g.DrawString(this.sbDamageLabel.ToString(), SystemFonts.DefaultFont, Brushes.Yellow, 30.0f, dmgYStart);
        g.DrawString(this.sbDamageValues.ToString(), SystemFonts.DefaultFont, Brushes.OrangeRed, 230.0f, dmgYStart);
      }
    }

    internal class PlayerTimingInfo
    {
      internal string name = null;
      internal double lastS1Time = -1.0;
      internal double lastS2Time = -1.0;
      internal double lastS3Time = -1.0;

      internal double currS1Time = -1.0;
      internal double currS2Time = -1.0;
      internal double currS3Time = -1.0;

      internal double bestS1Time = -1.0;
      internal double bestS2Time = -1.0;
      internal double bestS3Time = -1.0;

      internal double currLapET = -1.0;
      internal double lastLapTime = -1.0;
      internal double currLapTime = -1.0;
      internal double bestLapTime = -1.0;

      internal int currLap = -1;
    }

    internal class OpponentTimingInfo
    {
      internal string name = null;
      internal int position = -1;
      internal double lastS1Time = -1.0;
      internal double lastS2Time = -1.0;
      internal double lastS3Time = -1.0;

      internal double currS1Time = -1.0;
      internal double currS2Time = -1.0;
      internal double currS3Time = -1.0;

      internal double bestS1Time = -1.0;
      internal double bestS2Time = -1.0;
      internal double bestS3Time = -1.0;

      internal double currLapET = -1.0;
      internal double lastLapTime = -1.0;
      internal double currLapTime = -1.0;
      internal double bestLapTime = -1.0;

      internal int currLap = -1;

      internal string vehicleName = null;
      internal string vehicleClass = null;

      internal GTR2PitState pitState = GTR2PitState.None;
      internal int blueFlag = 0;

      internal long mID = -1;
    }

    // string -> lap data

    internal class LapData
    {
      internal class LapStats
      {
        internal int lapNumber = -1;
        internal double lapTime = -1.0;
        internal double S1Time = -1.0;
        internal double S2Time = -1.0;
        internal double S3Time = -1.0;
      }

      internal string name = null;
      internal int lastLapCompleted = -1;
      internal List<LapStats> lapStats = new List<LapStats>();
    }

    internal Dictionary<long, LapData> lapDataMap = null;
    int lastTimingSector = -1;
    string bestSplitString = "";

    public static Dictionary<long, int> idsToTelIndices = new Dictionary<long, int>();
    public static GTR2VehicleScoring playerVeh;

    private int GetSector(int GTR2Sector) { return GTR2Sector == 0 ? 3 : GTR2Sector; }
    public static string LapTimeStr(double time)
    {
      return time > 0.0 ? TimeSpan.FromSeconds(time).ToString(@"mm\:ss\:fff") : time.ToString();
    }

    internal void TrackTimings(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Extended extended, Graphics g, bool logToFile, int focusVehicleID)
    {
      if ((this.lastTimingTrackingGamePhase == GTR2GamePhase.Garage
            || this.lastTimingTrackingGamePhase == GTR2GamePhase.SessionOver
            || this.lastTimingTrackingGamePhase == GTR2GamePhase.SessionStopped
            || (int)this.lastTimingTrackingGamePhase == 9)  // What is 9? 
          && ((GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Countdown
            || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Formation
            || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GridWalk
            || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GreenFlag))
      {
        this.lapDataMap = null;
        this.bestSplitString = "";
        if (logToFile)
        {
          var lines = new List<string>();
          lines.Add("\n");
          lines.Add("************************************************************************************");
          lines.Add("* NEW SESSION **********************************************************************");
          lines.Add("************************************************************************************");
          File.AppendAllLines(timingTrackingFilePath, lines);
        }
      }

      this.lastTimingTrackingGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0
        || extended.mSessionStarted == 0)
      {
        this.lastTimingSector = -1;
        this.lapDataMap = null;
        this.bestSplitString = "";

        return;
      }

      if (this.lapDataMap == null)
        this.lapDataMap = new Dictionary<long, LapData>();

      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!TransitionTracker.idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = TransitionTracker.idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mPlayerTelemetry;

      bool sectorChanged = this.lastTimingSector != this.GetSector(TransitionTracker.playerVeh.mSector);
      bool newLap = this.lastTimingSector == 3 && this.GetSector(TransitionTracker.playerVeh.mSector) == 1;

      this.lastTimingSector = this.GetSector(TransitionTracker.playerVeh.mSector);

      StringBuilder sbPlayer = null;
      PlayerTimingInfo ptiPlayer = null;
      var bls = this.getBestLapStats(TransitionTracker.playerVeh.mID, newLap /*skipLastLap*/);
      this.getDetailedVehTiming("Player:", ref playerVeh, bls, ref scoring, out sbPlayer, out ptiPlayer);

      var opponentInfos = new List<OpponentTimingInfo>();
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        var veh = scoring.mVehicles[i];
        var o = new OpponentTimingInfo();
        o.mID = veh.mID;
        o.name = TransitionTracker.GetStringFromBytes(veh.mDriverName);
        o.position = veh.mPlace;

        o.lastS1Time = veh.mLastSector1 > 0.0 ? veh.mLastSector1 : -1.0;
        o.lastS2Time = veh.mLastSector1 > 0.0 && veh.mLastSector2 > 0.0
          ? veh.mLastSector2 - veh.mLastSector1 : -1.0;
        o.lastS3Time = veh.mLastSector2 > 0.0 && veh.mLastLapTime > 0.0
          ? veh.mLastLapTime - veh.mLastSector2 : -1.0;

        o.currS1Time = o.lastS1Time;
        o.currS2Time = o.lastS2Time;
        o.currS3Time = o.lastS3Time;

        // Check if we have more current values for S1 and S2.
        // S3 always equals to lastS3Time.
        if (veh.mCurSector1 > 0.0)
          o.currS1Time = veh.mCurSector1;

        if (veh.mCurSector1 > 0.0 && veh.mCurSector2 > 0.0)
          o.currS2Time = veh.mCurSector2 - veh.mCurSector1;

        o.bestS1Time = veh.mBestSector1 > 0.0 ? veh.mBestSector1 : -1.0;
        o.bestS2Time = veh.mBestSector1 > 0.0 && veh.mBestSector2 > 0.0 ? veh.mBestSector2 - veh.mBestSector1 : -1.0;

        // Wrong:
        o.bestS3Time = veh.mBestSector2 > 0.0 && veh.mBestLapTime > 0.0 ? veh.mBestLapTime - veh.mBestSector2 : -1.0;

        o.currLapET = veh.mLapStartET;
        o.lastLapTime = veh.mLastLapTime;
        o.currLapTime = scoring.mScoringInfo.mCurrentET - veh.mLapStartET;
        o.bestLapTime = veh.mBestLapTime;
        o.currLap = veh.mTotalLaps;
        o.vehicleName = TransitionTracker.GetStringFromBytes(veh.mVehicleName);
        o.vehicleClass = TransitionTracker.GetStringFromBytes(veh.mVehicleClass);

        var ev = extended.mExtendedVehicleScoring[veh.mID % MAX_MAPPED_IDS];
        o.pitState = (GTR2PitState)ev.mPitState;
        o.blueFlag = ev.mBlueFlag;

        opponentInfos.Add(o);
      }

      // Order by pos, ascending.
      opponentInfos.Sort((o1, o2) => o1.position.CompareTo(o2.position));
      var sbOpponentNames = new StringBuilder();
      sbOpponentNames.Append("Name | Class | Vehicle:\n");
      foreach (var o in opponentInfos)
      {
        if (focusVehicleID != o.mID)
          sbOpponentNames.Append($"{o.name} | {o.vehicleClass} | {o.vehicleName}\n");
        else
          sbOpponentNames.Append($">>> {o.name} | {o.vehicleClass} | {o.vehicleName}\n");
      }

      // Save lap times history.
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        var veh = scoring.mVehicles[i];
        var driverName = TransitionTracker.GetStringFromBytes(veh.mDriverName);

        // If we don't have this vehicle in a map, add it. (And initialize laps completed).
        if (!this.lapDataMap.ContainsKey(veh.mID))
        {
          var ldNew = new LapData();
          ldNew.lastLapCompleted = veh.mTotalLaps;
          ldNew.name = driverName;
          this.lapDataMap.Add(veh.mID, ldNew);
        }

        // If this is the new lap for this vehicle, update the lastLapNumber, and save last lap stats.
        var ld = this.lapDataMap[veh.mID];
        if (ld.lastLapCompleted != veh.mTotalLaps)
        {
          ld.lastLapCompleted = veh.mTotalLaps;

          // Only record valid laps.
          if (veh.mLastLapTime > 0.0)
          {
            var lsNew = new LapData.LapStats
            {
              lapNumber = veh.mTotalLaps,
              lapTime = veh.mLastLapTime,
              S1Time = veh.mLastSector1,
              S2Time = veh.mLastSector2 - veh.mLastSector1,
              S3Time = veh.mLastLapTime - veh.mLastSector2
            };

            ld.lapStats.Add(lsNew);
          }
        }
      }

      // TODO_GTR2:
      /*var idsToParticipantIndices = new Dictionary<long, int>();
      for (int i = 0; i < rules.mTrackRules.mNumParticipants; ++i)
      {
        if (!idsToParticipantIndices.ContainsKey(rules.mParticipants[i].mID))
          idsToParticipantIndices.Add(rules.mParticipants[i].mID, i);
      }*/

      var sbOpponentStats = new StringBuilder();
      sbOpponentStats.Append("Pos:  Lap: Best Trckd: Best S1:   Best S2:   Best S3:   Best Tel.:  Last Tel.:\n");
      foreach (var o in opponentInfos)
      {
        var skipLastLap = o.mID == TransitionTracker.playerVeh.mID && newLap;
        var bestLapStats = this.getBestLapStats((int)o.mID, skipLastLap);

        var bestLapS1 = bestLapStats.S1Time;
        var bestLapS2 = bestLapStats.S2Time;
        var bestLapS3 = bestLapStats.S3Time;
        var bestLapTimeTracked = bestLapStats.lapTime;

        sbOpponentStats.Append($"{o.position,3}{o.currLap,5}{TransitionTracker.LapTimeStr(bestLapTimeTracked),12:N3}{TransitionTracker.LapTimeStr(bestLapS1),12:N3}{TransitionTracker.LapTimeStr(bestLapS2),11:N3}{TransitionTracker.LapTimeStr(bestLapS3),11:N3}{TransitionTracker.LapTimeStr(o.bestLapTime),11:N3}{TransitionTracker.LapTimeStr(o.lastLapTime),12:N3}\n");
      }

      // Find fastest vehicle.
      var blsFastest = new LapData.LapStats();
      var fastestName = "";
      foreach (var lapData in this.lapDataMap)
      {
        // If this is the new lap, ignore just completed lap for the player vehicle, and use time of one lap before.
        bool skipLastLap = newLap && lapData.Key == TransitionTracker.playerVeh.mID;

        var blsCandidate = this.getBestLapStats((int)lapData.Key, skipLastLap);
        if (blsCandidate.lapTime < 0.0)
          continue;

        if (blsFastest.lapTime < 0.0
          || blsCandidate.lapTime < blsFastest.lapTime)
        {
          fastestName = lapData.Value.name;
          blsFastest = blsCandidate;
        }
      }

      int fastestIndex = -1;
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        if (fastestName == TransitionTracker.GetStringFromBytes(scoring.mVehicles[i].mDriverName))
        {
          fastestIndex = i;
          break;
        }
      }

      PlayerTimingInfo ptiFastest = null;
      var sbFastest = new StringBuilder("");
      if (fastestIndex != -1)
      {
        var fastestVeh = scoring.mVehicles[fastestIndex];
        if (blsFastest.lapTime > 0.0)
        {
          //'  var blsFastest = this.getBestLapStats(this.getStringFromBytes(fastestVeh.mDriverName));
          this.getDetailedVehTiming("Fastest:", ref fastestVeh, blsFastest, ref scoring, out sbFastest, out ptiFastest);
        }
      }

      var sbPlayerDeltas = new StringBuilder("");
      if (ptiFastest != null)
      {
        var deltaLapTime = ptiPlayer.bestLapTime - ptiFastest.bestLapTime;
        var deltaS1Time = ptiPlayer.bestS1Time - ptiFastest.bestS1Time;
        var deltaS2Time = ptiPlayer.bestS2Time - ptiFastest.bestS2Time;
        var deltaS3Time = ptiPlayer.bestS3Time - ptiFastest.bestS3Time;

        var deltaSelfLapTime = ptiPlayer.lastLapTime - ptiPlayer.bestLapTime;
        var deltaSelfS1Time = ptiPlayer.currS1Time - ptiPlayer.bestS1Time;
        var deltaSelfS2Time = ptiPlayer.currS2Time - ptiPlayer.bestS2Time;
        var deltaSelfS3Time = ptiPlayer.currS3Time - ptiPlayer.bestS3Time;

        var deltaCurrSelfLapTime = ptiPlayer.lastLapTime - ptiFastest.bestLapTime;
        var deltaCurrSelfS1Time = ptiPlayer.currS1Time - ptiFastest.bestS1Time;
        var deltaCurrSelfS2Time = ptiPlayer.currS2Time - ptiFastest.bestS2Time;
        var deltaCurrSelfS3Time = ptiPlayer.currS3Time - ptiFastest.bestS3Time;

        var deltaCurrSelfLapStr = deltaCurrSelfLapTime > 0.0 ? "+" : "";
        deltaCurrSelfLapStr = deltaCurrSelfLapStr + $"{deltaCurrSelfLapTime:N3}";

        var deltaCurrSelfS1Str = deltaCurrSelfS1Time > 0.0 ? "+" : "";
        deltaCurrSelfS1Str = deltaCurrSelfS1Str + $"{deltaCurrSelfS1Time:N3}";

        var deltaCurrSelfS2Str = deltaCurrSelfS2Time > 0.0 ? "+" : "";
        deltaCurrSelfS2Str = deltaCurrSelfS2Str + $"{deltaCurrSelfS2Time:N3}";

        var deltaCurrSelfS3Str = deltaCurrSelfS3Time > 0.0 ? "+" : "";
        deltaCurrSelfS3Str = deltaCurrSelfS3Str + $"{deltaCurrSelfS3Time:N3}";

        sbPlayerDeltas.Append($"Player delta current vs session best:    deltaCurrSelfBestLapTime: {deltaCurrSelfLapStr}\ndeltaCurrSelfBestS1: {deltaCurrSelfS1Str}    deltaCurrSelfBestS2: {deltaCurrSelfS2Str}    deltaCurrSelfBestS3: {deltaCurrSelfS3Str}\n\n");

        // Once per sector change.
        if (sectorChanged)
        {
          // Calculate "Best Split" to match rFactor 2 HUDs
          var currSector = this.GetSector(TransitionTracker.playerVeh.mSector);
          double bestSplit = 0.0;
          if (currSector == 1)
            bestSplit = ptiPlayer.lastLapTime - ptiFastest.bestLapTime;
          else if (currSector == 2)
            bestSplit = ptiPlayer.currS1Time - ptiFastest.bestS1Time;
          else
            bestSplit = (ptiPlayer.currS1Time + ptiPlayer.currS2Time) - (ptiFastest.bestS1Time + ptiFastest.bestS2Time);

          var bestSplitStr = bestSplit > 0.0 ? "+" : "";
          bestSplitStr += $"{bestSplit:N3}";

          this.bestSplitString = $"Best Split: {bestSplitStr}\n\n";
        }

        sbPlayerDeltas.Append(this.bestSplitString);

        var deltaSelfLapStr = deltaSelfLapTime > 0.0 ? "+" : "";
        deltaSelfLapStr = deltaSelfLapStr + $"{deltaSelfLapTime:N3}";

        var deltaSelfS1Str = deltaSelfS1Time > 0.0 ? "+" : "";
        deltaSelfS1Str = deltaSelfS1Str + $"{deltaSelfS1Time:N3}";

        var deltaSelfS2Str = deltaSelfS2Time > 0.0 ? "+" : "";
        deltaSelfS2Str = deltaSelfS2Str + $"{deltaSelfS2Time:N3}";

        var deltaSelfS3Str = deltaSelfS3Time > 0.0 ? "+" : "";
        deltaSelfS3Str = deltaSelfS3Str + $"{deltaSelfS3Time:N3}";

        sbPlayerDeltas.Append($"Player delta current vs self best:    deltaSelfBestLapTime: {deltaSelfLapStr}\ndeltaSelfBestS1: {deltaSelfS1Str}    deltaSelfBestS2: {deltaSelfS2Str}    deltaBestS3: {deltaSelfS3Str}\n\n");

        var deltaLapStr = deltaLapTime > 0.0 ? "+" : "";
        deltaLapStr = deltaLapStr + $"{deltaLapTime:N3}";

        var deltaS1Str = deltaS1Time > 0.0 ? "+" : "";
        deltaS1Str = deltaS1Str + $"{deltaS1Time:N3}";

        var deltaS2Str = deltaS2Time > 0.0 ? "+" : "";
        deltaS2Str = deltaS2Str + $"{deltaS2Time:N3}";

        var deltaS3Str = deltaS3Time > 0.0 ? "+" : "";
        deltaS3Str = deltaS3Str + $"{deltaS3Time:N3}";

        sbPlayerDeltas.Append($"Player delta best vs session best:    deltaBestLapTime: {deltaLapStr}\ndeltaBestS1: {deltaS1Str}    deltaBestS2: {deltaS2Str}    deltaBestS3: {deltaS3Str}\n\n");
      }

      if (logToFile && sectorChanged)
      {
        var updateTime = DateTime.Now.ToString();
        File.AppendAllText(timingTrackingFilePath, $"\n\n{updateTime}    Sector: {this.lastTimingSector}  ***************************************************** \n\n");

        File.AppendAllText(timingTrackingFilePath, sbPlayer.ToString() + "\n");
        File.AppendAllText(timingTrackingFilePath, sbPlayerDeltas.ToString() + "\n");
        File.AppendAllText(timingTrackingFilePath, sbFastest.ToString() + "\n");

        var names = sbOpponentNames.ToString().Split('\n');
        var stats = sbOpponentStats.ToString().Split('\n');
        var lines = new List<string>();
        Debug.Assert(names.Count() == stats.Count());

        for (int i = 0; i < names.Count(); ++i)
          lines.Add($"{stats[i]}    {names[i]}");

        File.AppendAllLines(timingTrackingFilePath, lines);
      }

      if (g != null && MainForm.instance.CurrentPage() == MainForm.Pages.PlayerVehDamageAndTimings)
      {
        var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
        var timingsYStart = this.screenYStart + 470.0f;
        g.DrawString(sbPlayer.ToString(), font, Brushes.Magenta, 3.0f, timingsYStart);
        g.DrawString(sbPlayerDeltas.ToString(), font, Brushes.Orange, 830.0f, timingsYStart);
        g.DrawString(sbFastest.ToString(), font, Brushes.OrangeRed, 3.0f, timingsYStart + 100);
        g.DrawString(sbOpponentNames.ToString(), font, Brushes.Yellow, 550.0f, 20.0f);
        g.DrawString(sbOpponentStats.ToString(), font, Brushes.OrangeRed, 1200.0f, 20.0f);
      }
    }

    private LapData.LapStats getBestLapStats(int id, bool skipLastLap)
    {
      LapData.LapStats bestLapStats = new LapData.LapStats();
      if (this.lapDataMap.ContainsKey(id))
      {
        var opLd = this.lapDataMap[id];

        double bestLapTimeTracked = -1.0;
        var lapsToCheck = opLd.lapStats.Count;
        if (skipLastLap)
          --lapsToCheck;

        for (int i = 0; i < lapsToCheck; ++i)
        {
          var ls = opLd.lapStats[i];
          if (bestLapStats.lapTime < 0.0
            || ls.lapTime < bestLapTimeTracked)
          {
            bestLapTimeTracked = ls.lapTime;
            bestLapStats = ls;
          }
        }
      }

      return bestLapStats;
    }

    private void getDetailedVehTiming(string name, ref GTR2VehicleScoring vehicle, LapData.LapStats bestLapStats, ref GTR2Scoring scoring, out StringBuilder sbDetails, out PlayerTimingInfo pti)
    {
      pti = new PlayerTimingInfo();
      pti.name = TransitionTracker.GetStringFromBytes(vehicle.mDriverName);
      pti.lastS1Time = vehicle.mLastSector1 > 0.0 ? vehicle.mLastSector1 : -1.0;
      pti.lastS2Time = vehicle.mLastSector1 > 0.0 && vehicle.mLastSector2 > 0.0
        ? vehicle.mLastSector2 - vehicle.mLastSector1 : -1.0;
      pti.lastS3Time = vehicle.mLastSector2 > 0.0 && vehicle.mLastLapTime > 0.0
        ? vehicle.mLastLapTime - vehicle.mLastSector2 : -1.0;

      pti.currS1Time = pti.lastS1Time;
      pti.currS2Time = pti.lastS2Time;
      pti.currS3Time = pti.lastS3Time;

      // Check if we have more current values for S1 and S2.
      // S3 always equals to lastS3Time.
      if (vehicle.mCurSector1 > 0.0)
        pti.currS1Time = vehicle.mCurSector1;

      if (vehicle.mCurSector1 > 0.0 && vehicle.mCurSector2 > 0.0)
        pti.currS2Time = vehicle.mCurSector2 - vehicle.mCurSector1;

      /*pti.bestS1Time = vehicle.mBestSector1 > 0.0 ? vehicle.mBestSector1 : -1.0;
      pti.bestS2Time = vehicle.mBestSector1 > 0.0 && vehicle.mBestSector2 > 0.0 ? vehicle.mBestSector2 - vehicle.mBestSector1 : -1.0;

      // This is not correct.  mBestLapTime does not neccessarily includes all three best sectors together.  The only way to calculate this is by continuous tracking.
      // However, currently there's no need for this value at all, so I don't care.
      pti.bestS3Time = vehicle.mBestSector2 > 0.0 && vehicle.mBestLapTime > 0.0 ? vehicle.mBestLapTime - vehicle.mBestSector2 : -1.0;*/

      // We need to skip previous player lap stats during comparison on new lap, hence we don't use vehicle values for those.
      pti.bestS1Time = bestLapStats.S1Time;
      pti.bestS2Time = bestLapStats.S2Time;
      pti.bestS3Time = bestLapStats.S3Time;
      pti.bestLapTime = bestLapStats.lapTime;

      pti.currLapET = vehicle.mLapStartET;
      pti.lastLapTime = vehicle.mLastLapTime;
      pti.currLapTime = scoring.mScoringInfo.mCurrentET - vehicle.mLapStartET;

      pti.currLap = vehicle.mTotalLaps;

      sbDetails = new StringBuilder();
      sbDetails.Append($"{name} {pti.name}\ncurrLapET: {TransitionTracker.LapTimeStr(pti.currLapET)}    lastLapTime: {TransitionTracker.LapTimeStr(pti.lastLapTime)}    currLapTime: {TransitionTracker.LapTimeStr(pti.currLapTime)}    bestLapTime: {TransitionTracker.LapTimeStr(pti.bestLapTime)}\n");
      sbDetails.Append($"lastS1: {TransitionTracker.LapTimeStr(pti.lastS1Time)}    lastS2: {TransitionTracker.LapTimeStr(pti.lastS2Time)}    lastS3: {TransitionTracker.LapTimeStr(pti.lastS3Time)}\n");
      sbDetails.Append($"currS1: {TransitionTracker.LapTimeStr(pti.currS1Time)}    currS2: {TransitionTracker.LapTimeStr(pti.currS2Time)}    currS3: {TransitionTracker.LapTimeStr(pti.currS3Time)}\n");
      sbDetails.Append($"bestS1: {TransitionTracker.LapTimeStr(pti.bestS1Time)}    bestS2: {TransitionTracker.LapTimeStr(pti.bestS2Time)}    bestS3: {TransitionTracker.LapTimeStr(pti.bestS3Time)}    bestTotal: {TransitionTracker.LapTimeStr(pti.bestS1Time + pti.bestS2Time + pti.bestS3Time)}\n");
    }
#if false
    internal class Rules
    {
      public GTR2TrackRulesStage mStage = (GTR2TrackRulesStage)Enum.ToObject(typeof(GTR2TrackRulesStage), -255);
      public GTR2TrackRulesColumn mPoleColumn = (GTR2TrackRulesColumn)Enum.ToObject(typeof(GTR2TrackRulesColumn), -255);      // column assignment where pole position seems to be located
      public int mNumActions = -1;                     // number of recent actions
      public int mNumParticipants = -1;                // number of participants (vehicles)

      public byte mYellowFlagDetected = 255;             // whether yellow flag was requested or sum of participant mYellowSeverity's exceeds mSafetyCarThreshold
      public byte mYellowFlagLapsWasOverridden = 255;    // whether mYellowFlagLaps (below) is an admin request

      public byte mSafetyCarExists = 255;                // whether safety car even exists
      public byte mSafetyCarActive = 255;                // whether safety car is active
      public int mSafetyCarLaps = 255;                  // number of laps
      public float mSafetyCarThreshold = -1.0f;            // the threshold at which a safety car is called out (compared to the sum of TrackRulesParticipantV01::mYellowSeverity for each vehicle)
      public double mSafetyCarLapDist;             // safety car lap distance
      public float mSafetyCarLapDistAtStart;       // where the safety car starts from

      public float mPitLaneStartDist = -1.0f;              // where the waypoint branch to the pits breaks off (this may not be perfectly accurate)
      public float mTeleportLapDist = -1.0f;               // the front of the teleport locations (a useful first guess as to where to throw the green flag)

      // input/output
      public sbyte mYellowFlagState = 127;         // see ScoringInfoV01 for values
      public short mYellowFlagLaps = 127;                // suggested number of laps to run under yellow (may be passed in with admin command)

      public GTR2SafetyCarInstruction mSafetyCarInstruction = (GTR2SafetyCarInstruction)Enum.ToObject(typeof(GTR2SafetyCarInstruction), -255);
      public float mSafetyCarSpeed = -1.0f;                // maximum speed at which to drive
      public float mSafetyCarMinimumSpacing = -2.0f;       // minimum spacing behind safety car (-1 to indicate no limit)
      public float mSafetyCarMaximumSpacing = -2.0f;       // maximum spacing behind safety car (-1 to indicate no limit)

      public float mMinimumColumnSpacing = -2.0f;          // minimum desired spacing between vehicles in a column (-1 to indicate indeterminate/unenforced)
      public float mMaximumColumnSpacing = -2.0f;          // maximum desired spacing between vehicles in a column (-1 to indicate indeterminate/unenforced)

      public float mMinimumSpeed = -2.0f;                  // minimum speed that anybody should be driving (-1 to indicate no limit)
      public float mMaximumSpeed = -2.0f;                  // maximum speed that anybody should be driving (-1 to indicate no limit)

      public string mMessage = "unknown";                  // a message for everybody to explain what is going on (which will get run through translator on client machines)

      public short mFrozenOrder = 127;                           // 0-based place when caution came out (not valid for formation laps)
      public short mPlace = 127;                                 // 1-based place (typically used for the initialization of the formation lap track order)
      public float mYellowSeverity = -1.0f;                        // a rating of how much this vehicle is contributing to a yellow flag (the sum of all vehicles is compared to TrackRulesV01::mSafetyCarThreshold)
      public double mCurrentRelativeDistance = -1.0;              // equal to ( ( ScoringInfoV01::mLapDist * this->mRelativeLaps ) + VehicleScoringInfoV01::mLapDist )

      // input/output
      public int mRelativeLaps = -1;                            // current formation/caution laps relative to safety car (should generally be zero except when safety car crosses s/f line); this can be decremented to implement 'wave around' or 'beneficiary rule' (a.k.a. 'lucky dog' or 'free pass')
      public GTR2TrackRulesColumn mColumnAssignment = (GTR2TrackRulesColumn)Enum.ToObject(typeof(GTR2TrackRulesColumn), -255);        // which column (line/lane) that participant is supposed to be in
      public int mPositionAssignment = -1;                      // 0-based position within column (line/lane) that participant is supposed to be located at (-1 is invalid)
      public byte mPitsOpen = 255;                           // whether the rules allow this particular vehicle to enter pits right now

      public double mGoalRelativeDistance = -1.0;                 // calculated based on where the leader is, and adjusted by the desired column spacing and the column/position assignments

      public string mMessage_Participant = "unknown";                  // a message for this participant to explain what is going on (untranslated; it will get run through translator on client machines)
    }

    internal Rules prevRules = new Rules();
    internal StringBuilder sbRulesChanged = new StringBuilder();
    internal StringBuilder sbRulesLabel = new StringBuilder();
    internal StringBuilder sbRulesValues = new StringBuilder();
    internal StringBuilder sbFrozenOrderInfo = new StringBuilder();
    internal StringBuilder sbFrozenOrderOnlineInfo = new StringBuilder();
    private FrozenOrderData prevFrozenOrderData;
    private FrozenOrderData prevFrozenOrderDataOnline;
    private long ticksLSIOrderInstructionMessageUpdated = 0L;
    private int numFODetectPhaseAttempts = 0;

    public enum FrozenOrderPhase
    {
      None,
      FullCourseYellow,
      FormationStanding,
      Rolling,
      FastRolling
    }

    public enum FrozenOrderColumn
    {
      None,
      Left,
      Right
    }

    public enum FrozenOrderAction
    {
      None,
      Follow,
      CatchUp,
      AllowToPass,
      StayInPole,  // Case of being assigned pole/pole row with no SC present (Rolling start in GTR2 Karts, for example).
      MoveToPole  // Case of falling behind assigned pole/pole row with no SC present (Rolling start in GTR2 Karts, for example).
    }

    public class FrozenOrderData
    {
      public FrozenOrderPhase Phase = FrozenOrderPhase.None;
      public FrozenOrderAction Action = FrozenOrderAction.None;

      // If column is assigned, p1 and p2 follows SC.  Otherwise,
      // only p1 follows SC.
      public int AssignedPosition = -1;

      public FrozenOrderColumn AssignedColumn = FrozenOrderColumn.None;
      // Only matters if AssignedColumn != None
      public int AssignedGridPosition = -1;

      public string DriverToFollow = "";

      // Meters/s.  If -1, SC either left or not present.
      public float SafetyCarSpeed = -1.0f;
    }

    // TODO_GTR2:
    internal void TrackRules(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Rules rules, ref GTR2Extended extended, Graphics g, bool logToFile)
    {
      if (logToFile)
      {
        if ((this.lastRulesTrackingGamePhase == GTR2GamePhase.Garage
              || this.lastRulesTrackingGamePhase == GTR2GamePhase.SessionOver
              || this.lastRulesTrackingGamePhase == GTR2GamePhase.SessionStopped
              || (int)this.lastRulesTrackingGamePhase == 9)  // What is 9? 
            && ((GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Countdown
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Formation
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GridWalk
              || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GreenFlag))
        {
          var lines = new List<string>();
          lines.Add("\n");
          lines.Add("************************************************************************************");
          lines.Add("* NEW SESSION **********************************************************************");
          lines.Add("************************************************************************************");
          File.AppendAllLines(rulesTrackingFilePath, lines);
          File.AppendAllLines(rulesTrackingDeltaFilePath, lines);
        }
      }

      this.lastRulesTrackingGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0)
        return;

      // Build map of mID -> telemetry.mVehicles[i]. 
      // They are typically matching values, however, we need to handle online cases and dropped vehicles (mID can be reused).
      var idsToTelIndices = new Dictionary<long, int>();
      for (int i = 0; i < telemetry.mNumVehicles; ++i)
      {
        if (!idsToTelIndices.ContainsKey(telemetry.mVehicles[i].mID))
          idsToTelIndices.Add(telemetry.mVehicles[i].mID, i);
      }

      var playerVeh = MainForm.GetPlayerScoring(ref scoring);

      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mVehicles[resolvedIdx];

      var playerRules = new GTR2TrackRulesParticipant();
      for (int i = 0; i < rules.mTrackRules.mNumParticipants; ++i)
      {
        if (rules.mParticipants[i].mID == scoringPlrId)
        {
          playerRules = rules.mParticipants[i];
          break;
        }
      }

      var rs = new Rules();

      rs.mStage = rules.mTrackRules.mStage;
      rs.mPoleColumn = rules.mTrackRules.mPoleColumn;
      rs.mNumActions = rules.mTrackRules.mNumActions;
      rs.mNumParticipants = rules.mTrackRules.mNumParticipants;
      rs.mYellowFlagDetected = rules.mTrackRules.mYellowFlagDetected;
      rs.mYellowFlagLapsWasOverridden = rules.mTrackRules.mYellowFlagLapsWasOverridden;
      rs.mSafetyCarExists = rules.mTrackRules.mSafetyCarExists;
      rs.mSafetyCarActive = rules.mTrackRules.mSafetyCarActive;
      rs.mSafetyCarLaps = rules.mTrackRules.mSafetyCarLaps;
      rs.mSafetyCarThreshold = rules.mTrackRules.mSafetyCarThreshold;
      rs.mSafetyCarLapDist = rules.mTrackRules.mSafetyCarLapDist;
      rs.mSafetyCarLapDistAtStart = rules.mTrackRules.mSafetyCarLapDistAtStart;
      rs.mPitLaneStartDist = rules.mTrackRules.mPitLaneStartDist;
      rs.mTeleportLapDist = rules.mTrackRules.mTeleportLapDist;
      rs.mYellowFlagState = rules.mTrackRules.mYellowFlagState;
      rs.mYellowFlagLaps = rules.mTrackRules.mYellowFlagLaps;
      rs.mSafetyCarInstruction = (GTR2SafetyCarInstruction)rules.mTrackRules.mSafetyCarInstruction;
      rs.mSafetyCarSpeed = rules.mTrackRules.mSafetyCarSpeed;
      rs.mSafetyCarMinimumSpacing = rules.mTrackRules.mSafetyCarMinimumSpacing;
      rs.mSafetyCarMaximumSpacing = rules.mTrackRules.mSafetyCarMaximumSpacing;
      rs.mMinimumColumnSpacing = rules.mTrackRules.mMinimumColumnSpacing;
      rs.mMaximumColumnSpacing = rules.mTrackRules.mMaximumColumnSpacing;
      rs.mMinimumSpeed = rules.mTrackRules.mMinimumSpeed;
      rs.mMaximumSpeed = rules.mTrackRules.mMaximumSpeed;
      rs.mMessage = TransitionTracker.GetStringFromBytes(rules.mTrackRules.mMessage);

      // Player specific:
      rs.mFrozenOrder = playerRules.mFrozenOrder;
      rs.mPlace = playerRules.mPlace;
      rs.mYellowSeverity = playerRules.mYellowSeverity;
      rs.mCurrentRelativeDistance = playerRules.mCurrentRelativeDistance;
      rs.mRelativeLaps = playerRules.mRelativeLaps;
      rs.mColumnAssignment = playerRules.mColumnAssignment;
      rs.mPositionAssignment = playerRules.mPositionAssignment;
      rs.mPitsOpen = playerRules.mPitsOpen;
      rs.mGoalRelativeDistance = playerRules.mGoalRelativeDistance;
      rs.mMessage_Participant = TransitionTracker.GetStringFromBytes(playerRules.mMessage);

      // Only refresh UI if there's change.
      // some fields are commented out because they change pretty much every frame.
      if (rs.mStage != this.prevRules.mStage
        || rs.mPoleColumn != this.prevRules.mPoleColumn
        || rs.mNumActions != this.prevRules.mNumActions
        || rs.mNumParticipants != this.prevRules.mNumParticipants
        || rs.mYellowFlagDetected != this.prevRules.mYellowFlagDetected
        || rs.mYellowFlagLapsWasOverridden != this.prevRules.mYellowFlagLapsWasOverridden
        || rs.mSafetyCarExists != this.prevRules.mSafetyCarExists
        || rs.mSafetyCarActive != this.prevRules.mSafetyCarActive
        || rs.mSafetyCarLaps != this.prevRules.mSafetyCarLaps
        || rs.mSafetyCarThreshold != this.prevRules.mSafetyCarThreshold
        //public double mSafetyCarLapDist             // safety car lap distance
        //public float mSafetyCarLapDistAtStart       // where the safety car starts from
        || rs.mPitLaneStartDist != this.prevRules.mPitLaneStartDist
        || rs.mTeleportLapDist != this.prevRules.mTeleportLapDist
        || rs.mYellowFlagState != this.prevRules.mYellowFlagState
        || rs.mYellowFlagLaps != this.prevRules.mYellowFlagLaps
        || rs.mSafetyCarInstruction != this.prevRules.mSafetyCarInstruction
        || rs.mSafetyCarSpeed != this.prevRules.mSafetyCarSpeed
        || rs.mSafetyCarMinimumSpacing != this.prevRules.mSafetyCarMinimumSpacing
        || rs.mSafetyCarMaximumSpacing != this.prevRules.mSafetyCarMaximumSpacing
        || rs.mMinimumColumnSpacing != this.prevRules.mMinimumColumnSpacing
        || rs.mMaximumColumnSpacing != this.prevRules.mMaximumColumnSpacing
        || rs.mMinimumSpeed != this.prevRules.mMinimumSpeed
        || rs.mMaximumSpeed != this.prevRules.mMaximumSpeed
        || rs.mMessage != this.prevRules.mMessage
        || rs.mFrozenOrder != this.prevRules.mFrozenOrder
        || rs.mPlace != this.prevRules.mPlace
        //|| rs.mYellowSeverity != this.prevRules.mYellowSeverity
        //|| rs.mCurrentRelativeDistance != this.prevRules.mCurrentRelativeDistance
        || rs.mRelativeLaps != this.prevRules.mRelativeLaps
        || rs.mColumnAssignment != this.prevRules.mColumnAssignment
        || rs.mPositionAssignment != this.prevRules.mPositionAssignment
        || rs.mPitsOpen != this.prevRules.mPitsOpen
        || rs.mGoalRelativeDistance != this.prevRules.mGoalRelativeDistance
        || rs.mMessage_Participant != this.prevRules.mMessage_Participant)
      {
        this.sbRulesChanged = new StringBuilder();
        sbRulesChanged.Append((rs.mStage != this.prevRules.mStage ? "***\n" : "\n")
          + (rs.mPoleColumn != this.prevRules.mPoleColumn ? "***\n" : "\n")
          + (rs.mNumActions != this.prevRules.mNumActions ? "***\n" : "\n")
          + (rs.mNumParticipants != this.prevRules.mNumParticipants ? "***\n" : "\n")
          + (rs.mYellowFlagDetected != this.prevRules.mYellowFlagDetected ? "***\n" : "\n")
          + (rs.mYellowFlagLapsWasOverridden != this.prevRules.mYellowFlagLapsWasOverridden ? "***\n" : "\n")
          + (rs.mSafetyCarExists != this.prevRules.mSafetyCarExists ? "***\n" : "\n")
          + (rs.mSafetyCarActive != this.prevRules.mSafetyCarActive ? "***\n" : "\n")
          + (rs.mSafetyCarLaps != this.prevRules.mSafetyCarLaps ? "***\n" : "\n")
          + (rs.mSafetyCarThreshold != this.prevRules.mSafetyCarThreshold ? "***\n" : "\n")
          + (rs.mSafetyCarLapDist != this.prevRules.mSafetyCarLapDist ? "***\n" : "\n")
          + (rs.mSafetyCarLapDistAtStart != this.prevRules.mSafetyCarLapDistAtStart ? "***\n" : "\n")
          + (rs.mPitLaneStartDist != this.prevRules.mPitLaneStartDist ? "***\n" : "\n")
          + (rs.mTeleportLapDist != this.prevRules.mTeleportLapDist ? "***\n" : "\n")
          + (rs.mYellowFlagState != this.prevRules.mYellowFlagState ? "***\n" : "\n")
          + (rs.mYellowFlagLaps != this.prevRules.mYellowFlagLaps ? "***\n" : "\n")
          + (rs.mSafetyCarInstruction != this.prevRules.mSafetyCarInstruction ? "***\n" : "\n")
          + (rs.mSafetyCarSpeed != this.prevRules.mSafetyCarSpeed ? "***\n" : "\n")
          + (rs.mSafetyCarMinimumSpacing != this.prevRules.mSafetyCarMinimumSpacing ? "***\n" : "\n")
          + (rs.mSafetyCarMaximumSpacing != this.prevRules.mSafetyCarMaximumSpacing ? "***\n" : "\n")
          + (rs.mMinimumColumnSpacing != this.prevRules.mMinimumColumnSpacing ? "***\n" : "\n")
          + (rs.mMaximumColumnSpacing != this.prevRules.mMaximumColumnSpacing ? "***\n" : "\n")
          + (rs.mMinimumSpeed != this.prevRules.mMinimumSpeed ? "***\n" : "\n")
          + (rs.mMaximumSpeed != this.prevRules.mMaximumSpeed ? "***\n" : "\n")
          + (rs.mMessage != this.prevRules.mMessage ? "***\n" : "\n")
          + (rs.mFrozenOrder != this.prevRules.mFrozenOrder ? "***\n" : "\n")
          + (rs.mPlace != this.prevRules.mPlace ? "***\n" : "\n")
          + (rs.mYellowSeverity != this.prevRules.mYellowSeverity ? "***\n" : "\n")
          + (rs.mCurrentRelativeDistance != this.prevRules.mCurrentRelativeDistance ? "***\n" : "\n")
          + (rs.mRelativeLaps != this.prevRules.mRelativeLaps ? "***\n" : "\n")
          + (rs.mColumnAssignment != this.prevRules.mColumnAssignment ? "***\n" : "\n")
          + (rs.mPositionAssignment != this.prevRules.mPositionAssignment ? "***\n" : "\n")
          + (rs.mPitsOpen != this.prevRules.mPitsOpen ? "***\n" : "\n")
          + (rs.mGoalRelativeDistance != this.prevRules.mGoalRelativeDistance ? "***\n" : "\n")
          + (rs.mMessage_Participant != this.prevRules.mMessage_Participant ? "***\n" : "\n"));

        // Save current Rules and state.
        this.prevRules = rs;

        this.sbRulesLabel = new StringBuilder();
        sbRulesLabel.Append("mStage:\n"
          + "mPoleColumn:\n"
          + "mNumActions:\n"
          + "mNumParticipants:\n"
          + "mYellowFlagDetected:\n"
          + "mYellowFlagLapsWasOverridden:\n"
          + "mSafetyCarExists:\n"
          + "mSafetyCarActive:\n"
          + "mSafetyCarLaps:\n"
          + "mSafetyCarThreshold:\n"
          + "mSafetyCarLapDist:\n"
          + "mSafetyCarLapDistAtStart:\n"
          + "mPitLaneStartDist:\n"
          + "mTeleportLapDist:\n"
          + "mYellowFlagState:\n"
          + "mYellowFlagLaps:\n"
          + "mSafetyCarInstruction:\n"
          + "mSafetyCarSpeed:\n"
          + "mSafetyCarMinimumSpacing:\n"
          + "mSafetyCarMaximumSpacing:\n"
          + "mMinimumColumnSpacing:\n"
          + "mMaximumColumnSpacing:\n"
          + "mMinimumSpeed:\n"
          + "mMaximumSpeed:\n"
          + "mMessage:\n"
          + "mFrozenOrder:\n"
          + "mPlace:\n"
          + "mYellowSeverity:\n"
          + "mCurrentRelativeDistance:\n"
          + "mRelativeLaps:\n"
          + "mColumnAssignment:\n"
          + "mPositionAssignment:\n"
          + "mPitsOpen:\n"
          + "mGoalRelativeDistance:\n"
          + "mMessage_Participant:\n");

        this.sbRulesValues = new StringBuilder();
        sbRulesValues.Append($"{rs.mStage}\n"
          + $"{rs.mPoleColumn}\n"
          + $"{rs.mNumActions}\n"
          + $"{rs.mNumParticipants}\n"
          + $"{rs.mYellowFlagDetected}\n"
          + (rs.mYellowFlagLapsWasOverridden == 0 ? $"false({rs.mYellowFlagLapsWasOverridden})" : $"true({rs.mYellowFlagLapsWasOverridden})") + "\n"
          + (rs.mSafetyCarExists == 0 ? $"false({rs.mSafetyCarExists})" : $"true({rs.mSafetyCarExists})") + "\n"
          + (rs.mSafetyCarActive == 0 ? $"false({rs.mSafetyCarActive})" : $"true({rs.mSafetyCarActive})") + "\n"
          + $"{rs.mSafetyCarLaps}\n"
          + $"{rs.mSafetyCarThreshold:N3}\n"
          + $"{rs.mSafetyCarLapDist:N3}\n"
          + $"{rs.mSafetyCarLapDistAtStart:N3}\n"
          + $"{rs.mPitLaneStartDist:N3}\n"
          + $"{rs.mTeleportLapDist:N3}\n"
          + $"{GetEnumString<GTR2YellowFlagState>(rs.mYellowFlagState)}\n"
          + $"{rs.mYellowFlagLaps}\n"
          + $"{rs.mSafetyCarInstruction}\n"
          + $"{rs.mSafetyCarSpeed:N3}\n"
          + $"{rs.mSafetyCarMinimumSpacing:N3}\n"
          + $"{rs.mSafetyCarMaximumSpacing:N3}\n"
          + $"{rs.mMinimumColumnSpacing:N3}\n"
          + $"{rs.mMaximumColumnSpacing}\n"
          + $"{rs.mMinimumSpeed:N3}\n"
          + $"{rs.mMaximumSpeed:N3}\n"
          + $"{rs.mMessage}\n"
          + $"{rs.mFrozenOrder}\n"
          + $"{rs.mPlace}\n"
          + $"{rs.mYellowSeverity:N3}\n"
          + $"{rs.mCurrentRelativeDistance:N3}\n"
          + $"{rs.mRelativeLaps}\n"
          + $"{rs.mColumnAssignment}\n"
          + $"{rs.mPositionAssignment}\n"
          + $"{rs.mPitsOpen}\n"
          + (rs.mGoalRelativeDistance > 20000 ? $"too large" : $"{rs.mGoalRelativeDistance:N3})") + "\n"
          + $"{rs.mMessage_Participant}\n");

        if (logToFile)
        {
          var changed = this.sbRulesChanged.ToString().Split('\n');
          var labels = this.sbRulesLabel.ToString().Split('\n');
          var values = this.sbRulesValues.ToString().Split('\n');

          var list = new List<string>(changed);
          changed = list.ToArray();

          list = new List<string>(labels);
          labels = list.ToArray();

          list = new List<string>(values);
          values = list.ToArray();

          Debug.Assert(changed.Length == labels.Length && values.Length == labels.Length);

          var lines = new List<string>();
          var updateTime = DateTime.Now.ToString();

          lines.Add($"\n{updateTime}");
          for (int i = 0; i < changed.Length; ++i)
            lines.Add($"{changed[i]}{labels[i]}{values[i]}");

          File.AppendAllLines(rulesTrackingFilePath, lines);

          lines.Clear();

          lines.Add($"\n{updateTime}");
          for (int i = 0; i < changed.Length; ++i)
          {
            if (changed[i].StartsWith("***"))
              lines.Add($"{changed[i]}{labels[i]}{values[i]}");
          }

          File.AppendAllLines(rulesTrackingDeltaFilePath, lines);
        }
      }

      var distToSC = -1.0;
      var fod = this.GetFrozenOrderData(prevFrozenOrderData, ref playerVeh, ref scoring, ref playerRules, ref rules, ref extended, ref distToSC);
      prevFrozenOrderData = fod;

      var driverToFollow = fod.DriverToFollow;
      if (((fod.AssignedColumn == FrozenOrderColumn.None && fod.AssignedPosition == 1)  // Single file order.
          || (fod.AssignedColumn != FrozenOrderColumn.None && fod.AssignedPosition <= 2))  // Double file (grid) order.
        && fod.SafetyCarSpeed > 0.0)
        driverToFollow = "Safety Car";

      this.sbFrozenOrderInfo = new StringBuilder();
      this.sbFrozenOrderInfo.Append(
        $"Frozen Order Phase: {fod.Phase}\n"
        + $"Frozen Order Action: {fod.Action}\n"
        + $"Assigned Position: {fod.AssignedPosition}\n"
        + $"Assigned Column: {fod.AssignedColumn}\n"
        + $"Assigned Grid Position: {fod.AssignedGridPosition}\n"
        + $"Driver To Follow: {driverToFollow}\n"
        + $"Safety Car Speed: {(fod.SafetyCarSpeed == -1.0f ? "Not Present" : string.Format("{0:N3}km/h", fod.SafetyCarSpeed * 3.6f))}\n"
        + $"SCR DoubleFileType: {extended.mSCRPluginDoubleFileType}\n"
        + $"Distance To Safety Car: {distToSC:N3}\n"
        );

      var distToSCOnline = -1.0;
      var fodOnline = this.GetFrozenOrderOnlineData(prevFrozenOrderDataOnline, ref playerVeh, ref scoring, ref extended, ref distToSCOnline);
      prevFrozenOrderDataOnline = fodOnline;
      if (fodOnline != null)
      {
        var driverToFollowOnline = fodOnline.DriverToFollow;
        if (((fodOnline.AssignedColumn == FrozenOrderColumn.None && fodOnline.AssignedPosition == 1)  // Single file order.
            || (fodOnline.AssignedColumn != FrozenOrderColumn.None && fodOnline.AssignedPosition <= 2))  // Double file (grid) order.
          && fodOnline.SafetyCarSpeed > 0.0)
          driverToFollowOnline = "Safety Car";

        this.sbFrozenOrderOnlineInfo = new StringBuilder();
        this.sbFrozenOrderOnlineInfo.Append(
          $"Frozen Order Phase: {fodOnline.Phase}\n"
          + $"Frozen Order Action: {fodOnline.Action}\n"
          + $"Assigned Position: {fodOnline.AssignedPosition}\n"
          + $"Assigned Column: {fodOnline.AssignedColumn}\n"
          + $"Assigned Grid Position: {fodOnline.AssignedGridPosition}\n"
          + $"Driver To Follow: {driverToFollowOnline}\n"
          + $"Safety Car Speed: {(fodOnline.SafetyCarSpeed == -1.0f ? "Not Present" : string.Format("{0:N3}km/h", fodOnline.SafetyCarSpeed * 3.6f))}\n"
          + $"SCR DoubleFileType: {extended.mSCRPluginDoubleFileType}\n"
          + $"Distance To Safety Car: {distToSCOnline:N3}\n"
          );
      }
      else
        this.sbFrozenOrderOnlineInfo.Clear();

      if (g != null)
      {
        float rulesY = 3.0f;
        float rulesX = 1600.0f;
        g.DrawString(this.sbRulesChanged.ToString(), SystemFonts.DefaultFont, Brushes.Orange, rulesX, rulesY);
        g.DrawString(this.sbRulesLabel.ToString(), SystemFonts.DefaultFont, Brushes.Green, rulesX + 30.0f, rulesY);
        g.DrawString(this.sbRulesValues.ToString(), SystemFonts.DefaultFont, Brushese, rulesX + 200.0f, rulesY);
        g.DrawString(this.sbFrozenOrderInfo.ToString(), SystemFonts.DefaultFont, Brushes.DarkCyan, rulesX - 100.0f, rulesY + 450);
        g.DrawString(this.sbFrozenOrderOnlineInfo.ToString(), SystemFonts.DefaultFont, Brushes.Blue, rulesX + 100.0f, rulesY + 450);
      }
    }

    private FrozenOrderData GetFrozenOrderData(FrozenOrderData prevFrozenOrderData, ref GTR2VehicleScoring vehicle,
      ref GTR2Scoring scoring, ref GTR2TrackRulesParticipant vehicleRules, ref GTR2Rules rules, ref GTR2Extended extended, ref double distToSC)
    {
      var fod = new FrozenOrderData();

      // Only applies to formation laps and FCY.
      if (scoring.mScoringInfo.mGamePhase != (int)GTR2GamePhase.Formation
        && scoring.mScoringInfo.mGamePhase != (int)GTR2GamePhase.FullCourseYellow)
      {
        this.numFODetectPhaseAttempts = 0;
        return fod;
      }

      var foStage = rules.mTrackRules.mStage;
      if (foStage == GTR2TrackRulesStage.Normal)
        return fod; // Note, there's slight race between scoring and rules here, FO messages should have validation on them.

      if (extended.mDirectMemoryAccessEnabled != 0)
      {
        if (prevFrozenOrderData == null || prevFrozenOrderData.Phase == FrozenOrderPhase.None)
        {
          // Don't bother checking updated ticks, this showld allow catching multiple SC car phases.
          var phase = TransitionTracker.GetStringFromBytes(extended.mLSIPhaseMessage);

          if (scoring.mScoringInfo.mGamePhase == (int)GTR2GamePhase.Formation
            && string.IsNullOrWhiteSpace(phase))
          {
            if (this.numFODetectPhaseAttempts > 0)
              fod.Phase = FrozenOrderPhase.FormationStanding;

            ++this.numFODetectPhaseAttempts;
          }
          else if (!string.IsNullOrWhiteSpace(phase)
            && phase == "Formation Lap")
          {
            var speed = Math.Sqrt((vehicle.mLocalVel.x * vehicle.mLocalVel.x)
              + (vehicle.mLocalVel.y * vehicle.mLocalVel.y)
              + (vehicle.mLocalVel.z * vehicle.mLocalVel.z));

            fod.Phase = this.GetSector(vehicle.mSector) == 3 && speed > 10.0 ? FrozenOrderPhase.FastRolling : FrozenOrderPhase.Rolling;
          }
          else if (!string.IsNullOrWhiteSpace(phase)
            && (phase == "Full-Course Yellow" || phase == "One Lap To Go"))
            fod.Phase = FrozenOrderPhase.FullCourseYellow;
          else if (string.IsNullOrWhiteSpace(phase))
            fod.Phase = prevFrozenOrderData.Phase;
        }
        else
          fod.Phase = prevFrozenOrderData.Phase;
      }
      else
      {
        // GTR2 currently does not expose what kind of race start is chosen.  For tracks with SC, I use presence of SC to distinguish between
        // Formation/Standing and Rolling starts.  However, if SC does not exist (Kart tracks), I used the fact that in Rolling start leader is
        // typically standing past S/F line (mLapDist is positive).  Obviously, there will be perverted tracks where that won't be true, but this
        // all I could come up with, and real problem is in game being shit in this area.
        var leaderLapDistAtFOPhaseStart = 0.0;
        var leaderSectorAtFOPhaseStart = -1;
        if (foStage != GTR2TrackRulesStage.CautionInit && foStage != GTR2TrackRulesStage.CautionUpdate  // If this is not FCY.
          && (prevFrozenOrderData == null || prevFrozenOrderData.Phase == FrozenOrderPhase.None)  // And, this is first FO calculation.
          && rules.mTrackRules.mSafetyCarExists == 0) // And, track has no SC.
        {
          // Find where leader is relatively to F/S line.
          for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
          {
            var veh = scoring.mVehicles[i];
            if (veh.mPlace == 1)
            {
              leaderLapDistAtFOPhaseStart = veh.mLapDist;
              leaderSectorAtFOPhaseStart = this.GetSector(veh.mSector);
              break;
            }
          }
        }

        // Figure out the phase:
        if (foStage == GTR2TrackRulesStage.CautionInit || foStage == GTR2TrackRulesStage.CautionUpdate)
          fod.Phase = FrozenOrderPhase.FullCourseYellow;
        else if (foStage == GTR2TrackRulesStage.FormationInit || foStage == GTR2TrackRulesStage.FormationUpdate)
        {
          // Check for signs of a rolling start.
          if ((prevFrozenOrderData != null && prevFrozenOrderData.Phase == FrozenOrderPhase.Rolling)  // If FO started as Rolling, keep it as Rolling even after SC leaves the track
            || (rules.mTrackRules.mSafetyCarExists == 1 && rules.mTrackRules.mSafetyCarActive == 1)  // Of, if SC exists and is active
            || (rules.mTrackRules.mSafetyCarExists == 0 && leaderLapDistAtFOPhaseStart > 0.0 && leaderSectorAtFOPhaseStart == 1)) // Or, if SC is not present on a track, and leader started ahead of S/F line and is insector 1.  This will be problem on some tracks.
            fod.Phase = FrozenOrderPhase.Rolling;
          else
          {
            // Formation / Standing and Fast Rolling have no Safety Car.
            fod.Phase = rules.mTrackRules.mStage == GTR2TrackRulesStage.FormationInit && this.GetSector(vehicle.mSector) == 3
              ? FrozenOrderPhase.FastRolling  // Fast rolling never goes into FormationUpdate and usually starts in S3.
              : FrozenOrderPhase.FormationStanding;
          }
        }
      }

      if (fod.Phase == FrozenOrderPhase.None)
        return fod;  // Wait a bit, there's a delay for string based phases.

      if (vehicleRules.mPositionAssignment != -1)
      {
        var gridOrder = false;

        var scrLastLapDoubleFile = fod.Phase == FrozenOrderPhase.FullCourseYellow
          && extended.mSCRPluginEnabled == 1
          && (extended.mSCRPluginDoubleFileType == 1 || extended.mSCRPluginDoubleFileType == 2)
          && scoring.mScoringInfo.mYellowFlagState == (sbyte)GTR2YellowFlagState.LastLap;

        if (fod.Phase == FrozenOrderPhase.FullCourseYellow  // Core FCY does not use grid order. 
          && !scrLastLapDoubleFile)  // With SCR rules, however, last lap might be double file depending on DoubleFileType configuration var value.
        {
          gridOrder = false;
          fod.AssignedPosition = vehicleRules.mPositionAssignment + 1;  // + 1, because it is zero based with 0 meaning follow SC.
        }
        else  // This is not FCY, or last lap of Double File FCY with SCR plugin enabled.  The order reported is grid order, with columns specified.
        {
          gridOrder = true;
          fod.AssignedGridPosition = vehicleRules.mPositionAssignment + 1;
          fod.AssignedColumn = vehicleRules.mColumnAssignment == GTR2TrackRulesColumn.LeftLane ? FrozenOrderColumn.Left : FrozenOrderColumn.Right;

          if (rules.mTrackRules.mPoleColumn == GTR2TrackRulesColumn.LeftLane)
          {
            fod.AssignedPosition = (vehicleRules.mColumnAssignment == GTR2TrackRulesColumn.LeftLane
              ? vehicleRules.mPositionAssignment * 2
              : vehicleRules.mPositionAssignment * 2 + 1) + 1;
          }
          else if (rules.mTrackRules.mPoleColumn == GTR2TrackRulesColumn.RightLane)
          {
            fod.AssignedPosition = (vehicleRules.mColumnAssignment == GTR2TrackRulesColumn.RightLane
              ? vehicleRules.mPositionAssignment * 2
              : vehicleRules.mPositionAssignment * 2 + 1) + 1;
          }

        }

        // Figure out Driver Name to follow.
        // NOTE: In Formation/Standing, game does not report those in UI, but we could.
        var vehToFollowId = -1;
        var followSC = true;
        if ((gridOrder && fod.AssignedPosition > 2)  // In grid order, first 2 vehicles are following SC.
          || (!gridOrder && fod.AssignedPosition > 1))  // In non-grid order, 1st car is following SC.
        {
          followSC = false;
          // Find the mID of a vehicle in front of us by frozen order.
          for (int i = 0; i < rules.mTrackRules.mNumParticipants; ++i)
          {
            var p = rules.mParticipants[i];
            if ((!gridOrder  // Don't care about column in non-grid order case.
                || (gridOrder && p.mColumnAssignment == vehicleRules.mColumnAssignment))  // Should be vehicle in the same column.
              && p.mPositionAssignment == (vehicleRules.mPositionAssignment - 1))
            {
              vehToFollowId = p.mID;
              break;
            }
          }
        }

        var playerDist = this.GetDistanceCompleteded(ref scoring, ref vehicle);
        var toFollowDist = -1.0;

        if (!followSC)
        {
          // Now find the vehicle to follow from the scoring info.
          for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
          {
            var v = scoring.mVehicles[i];
            if (v.mID == vehToFollowId)
            {
              fod.DriverToFollow = TransitionTracker.GetStringFromBytes(v.mDriverName);

              toFollowDist = this.GetDistanceCompleteded(ref scoring, ref v);
              break;
            }
          }
        }
        else
          toFollowDist = ((vehicle.mTotalLaps - vehicleRules.mRelativeLaps) * scoring.mScoringInfo.mLapDist) + rules.mTrackRules.mSafetyCarLapDist;

        distToSC = rules.mTrackRules.mSafetyCarActive == 1
          ? (((vehicle.mTotalLaps - vehicleRules.mRelativeLaps) * scoring.mScoringInfo.mLapDist) + rules.mTrackRules.mSafetyCarLapDist) - playerDist
          : -1.0;

        if (fod.Phase == FrozenOrderPhase.Rolling
          && followSC 
          && rules.mTrackRules.mSafetyCarExists == 0)
        {
          // Find distance to car next to us if we're in pole.
          var neighborDist = -1.0;
          for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
          {
            var veh = scoring.mVehicles[i];
            if (veh.mPlace == (vehicle.mPlace == 1 ? 2 : 1))
            {
              neighborDist = this.GetDistanceCompleteded(ref scoring, ref veh);
              break;
            }
          }

          var distDelta = neighborDist - playerDist;
          // Special case if we have to stay in pole row, but there's no SC on this track.
          if (fod.AssignedColumn == FrozenOrderColumn.None)
            fod.Action = distDelta > 70.0 ? FrozenOrderAction.MoveToPole : FrozenOrderAction.StayInPole;
          else
            fod.Action = distDelta > 70.0 ? FrozenOrderAction.MoveToPole : FrozenOrderAction.StayInPole;
        }
        else
        {
          Debug.Assert(toFollowDist != -1.0);

          fod.Action = FrozenOrderAction.Follow;

          var distDelta = toFollowDist - playerDist;
          if (distDelta < 0.0)
            fod.Action = FrozenOrderAction.AllowToPass;
          else if (distDelta > 70.0)
            fod.Action = FrozenOrderAction.CatchUp;
        }
      }

      if (rules.mTrackRules.mSafetyCarActive == 1)
        fod.SafetyCarSpeed = rules.mTrackRules.mSafetyCarSpeed;

      return fod;
    }

    private FrozenOrderData GetFrozenOrderOnlineData(FrozenOrderData prevFrozenOrderData, ref GTR2VehicleScoring vehicle,
      ref GTR2Scoring scoring, ref GTR2Extended extended, ref double distToSC)
    {
      if (extended.mDirectMemoryAccessEnabled == 0)
        return null;

      var fod = new FrozenOrderData();

      // Only applies to formation laps and FCY.
      if (scoring.mScoringInfo.mGamePhase != (int)GTR2GamePhase.Formation
        && scoring.mScoringInfo.mGamePhase != (int)GTR2GamePhase.FullCourseYellow)
      {
        this.numFODetectPhaseAttempts = 0;
        return fod;
      }

      if (prevFrozenOrderData != null)
      {
        // Carry old state over.
        fod.Action = prevFrozenOrderData.Action;
        fod.AssignedColumn = prevFrozenOrderData.AssignedColumn;
        fod.AssignedPosition = prevFrozenOrderData.AssignedPosition;
        fod.AssignedGridPosition = prevFrozenOrderData.AssignedGridPosition;
        fod.DriverToFollow = prevFrozenOrderData.DriverToFollow;
        fod.Phase = prevFrozenOrderData.Phase;
        fod.SafetyCarSpeed = prevFrozenOrderData.SafetyCarSpeed;
      }

      if (fod.Phase == FrozenOrderPhase.None)
      {
        // Don't bother checking updated ticks, this showld allow catching multiple SC car phases.
        var phase = TransitionTracker.GetStringFromBytes(extended.mLSIPhaseMessage);

        if (scoring.mScoringInfo.mGamePhase == (int)GTR2GamePhase.Formation
          && string.IsNullOrWhiteSpace(phase))
        {
          if (this.numFODetectPhaseAttempts > 0)
            fod.Phase = FrozenOrderPhase.FormationStanding;

          ++this.numFODetectPhaseAttempts;
        }
        else if (!string.IsNullOrWhiteSpace(phase)
          && phase == "Formation Lap")
        {
          var speed = Math.Sqrt((vehicle.mLocalVel.x * vehicle.mLocalVel.x)
            + (vehicle.mLocalVel.y * vehicle.mLocalVel.y)
            + (vehicle.mLocalVel.z * vehicle.mLocalVel.z));

          fod.Phase = this.GetSector(vehicle.mSector) == 3 && speed > 10.0 ? FrozenOrderPhase.FastRolling : FrozenOrderPhase.Rolling;
        }
        else if (!string.IsNullOrWhiteSpace(phase)
          && (phase == "Full-Course Yellow" || phase == "One Lap To Go"))
          fod.Phase = FrozenOrderPhase.FullCourseYellow;
        else if (string.IsNullOrWhiteSpace(phase))
          fod.Phase = prevFrozenOrderData.Phase;
      }

      if (fod.Phase == FrozenOrderPhase.None)
        return fod;  // Wait a bit, there's a delay for string based phases.

      // NOTE: for formation/standing capture order once.   For other phases, rely on LSI text.
      if ((fod.Phase == FrozenOrderPhase.FastRolling || fod.Phase == FrozenOrderPhase.Rolling || fod.Phase == FrozenOrderPhase.FullCourseYellow)
        && this.ticksLSIOrderInstructionMessageUpdated != extended.mTicksLSIOrderInstructionMessageUpdated)
      {
        this.ticksLSIOrderInstructionMessageUpdated = extended.mTicksLSIOrderInstructionMessageUpdated;

        var orderInstruction = TransitionTracker.GetStringFromBytes(extended.mLSIOrderInstructionMessage);
        if (!string.IsNullOrWhiteSpace(orderInstruction))
        {
          var followPrefix = @"Please Follow ";
          var catchUpToPrefix = @"Please Catch Up To ";
          var allowToPassPrefix = @"Please Allow ";
          
          var action = FrozenOrderAction.None;

          string prefix = null;
          if (orderInstruction.StartsWith(followPrefix))
          {
            prefix = followPrefix;
            action = FrozenOrderAction.Follow;
          }
          else if (orderInstruction.StartsWith(catchUpToPrefix))
          {
            prefix = catchUpToPrefix;
            action = FrozenOrderAction.CatchUp;
          }
          else if (orderInstruction.StartsWith(allowToPassPrefix))
          {
            prefix = allowToPassPrefix;
            action = FrozenOrderAction.AllowToPass;
          }
          else
            Debug.Assert(false, "unhandled action");

          if (!string.IsNullOrWhiteSpace(prefix))
          {
            var closingQuoteIdx = orderInstruction.LastIndexOf("\"");
            string driverName = null;
            try
            {
              if (closingQuoteIdx != -1)
              {
                driverName = orderInstruction.Substring(prefix.Length + 1, closingQuoteIdx - prefix.Length - 1);
              }
              else
              {
                driverName = "Safety Car";
              }
            }
            catch (Exception){}

            // Remove [-0.2 laps] if it is there.
            var lastOpenBckt = orderInstruction.LastIndexOf('[');
            if (lastOpenBckt != -1)
            {
              try
              {
                orderInstruction = orderInstruction.Substring(0, lastOpenBckt - 1);
              }
              catch (Exception) {}
            }

            var column = FrozenOrderColumn.None;
            if (orderInstruction.EndsWith("(In Right Line)"))
              column = FrozenOrderColumn.Right;
            else if (orderInstruction.EndsWith("(In Left Line)"))
              column = FrozenOrderColumn.Left;
            else if (!orderInstruction.EndsWith("\"") && action != FrozenOrderAction.AllowToPass)
              Debug.Assert(false, "unrecognized postfix");

            // Note: assigned Grid position only matters for Formation/Standing - don't bother figuring it out, just figure out assigned position (starting position).
            var assignedPos = -1;
            if (!string.IsNullOrWhiteSpace(driverName))
            {
              if (driverName != "Safety Car")
              {
                for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
                {
                  var veh = scoring.mVehicles[i];
                  var driver = TransitionTracker.GetStringFromBytes(veh.mDriverName);
                  if (driver == driverName)
                  {
                    if (column == FrozenOrderColumn.None)
                    {
                      assignedPos = action == FrozenOrderAction.Follow || action == FrozenOrderAction.CatchUp
                        ? veh.mPlace + 1
                        : veh.mPlace - 1; // Might not be true
                    }
                    else
                    {
                      assignedPos = action == FrozenOrderAction.Follow || action == FrozenOrderAction.CatchUp
                        ? veh.mPlace + 2
                        : veh.mPlace - 2; // Might not be true
                    }
                    break;
                  }
                }
              }
              else
              {
                assignedPos = vehicle.mPlace;
              }
            }

            fod.Action = action;
            fod.AssignedColumn = column;
            fod.DriverToFollow = driverName;
            fod.AssignedPosition = assignedPos;
          }
        }
      }
      else if ((prevFrozenOrderData == null || prevFrozenOrderData.Phase == FrozenOrderPhase.None)
        && fod.Phase == FrozenOrderPhase.FormationStanding)
      {
        // Just capture the starting position.
        fod.AssignedColumn = vehicle.mTrackEdge > 0.0 ? FrozenOrderColumn.Right : FrozenOrderColumn.Left;
        fod.AssignedPosition = vehicle.mPlace;

        // We need to know which side of a grid leader is here, gosh what a bullshit.
        // Find where leader is relatively to F/S line.
        var leaderCol = FrozenOrderColumn.None;
        for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
        {
          var veh = scoring.mVehicles[i];
          if (veh.mPlace == 1)
          {
            leaderCol = veh.mTrackEdge > 0.0 ? FrozenOrderColumn.Right : FrozenOrderColumn.Left;
            break;
          }
        }

        if (fod.AssignedColumn == FrozenOrderColumn.Left)
        {
          fod.AssignedGridPosition = leaderCol == FrozenOrderColumn.Left 
            ? (vehicle.mPlace / 2) + 1 
            : vehicle.mPlace / 2;
        }
        else if (fod.AssignedColumn == FrozenOrderColumn.Right)
        {
          fod.AssignedGridPosition = leaderCol == FrozenOrderColumn.Right
            ? (vehicle.mPlace / 2) + 1
            : vehicle.mPlace / 2;
        }
      }

      return fod;
    }
#endif
    private double GetDistanceCompleteded(ref GTR2Scoring scoring, ref GTR2VehicleScoring vehicle)
    {
      // Note: Can be interpolated a bit.
      return vehicle.mTotalLaps * scoring.mScoringInfo.mLapDist + vehicle.mLapDist;
    }
  }
}
