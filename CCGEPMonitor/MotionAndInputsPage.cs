﻿using GTR2SharedMemory.GTR2Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GTR2SharedMemory.GTR2Constants;

namespace CCGEPMonitor
{
  class MotionAndInputsPage
  {
    internal class OpponentMotionAndInputsInfo
    {
      internal string name = null;
      internal int position = -1;
      internal string vehicleName = null;
      internal string vehicleClass = null;
      internal float worldX = 0.0f;
      internal float worldY = 0.0f;
      internal float worldZ = 0.0f;
      internal float yaw = 0.0f;
      internal float pitch = 0.0f;
      internal float roll = 0.0f;
      internal float speedKMH = 0.0f;
      internal float rpm = 0.0f;
      internal byte gear = 0;
      internal float steeringInput = 0.0f;
      internal float throttleInput = 0.0f;
      internal float brakeInput = 0.0f;

      internal GTR2PitState pitState = GTR2PitState.None;
      internal int blueFlag = 0;

      internal long mID = -1;
    }

    GTR2GamePhase lastMotionAndInputsTrackingGamePhase = (GTR2GamePhase)Enum.ToObject(typeof(GTR2GamePhase), -255);
    private float screenYStart = 293.0f;

    internal void RenderPage(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Extended extended, int focusVehicleID, Graphics g)
    {
      if ((this.lastMotionAndInputsTrackingGamePhase == GTR2GamePhase.Garage
        || this.lastMotionAndInputsTrackingGamePhase == GTR2GamePhase.SessionOver
        || this.lastMotionAndInputsTrackingGamePhase == GTR2GamePhase.SessionStopped
        || (int)this.lastMotionAndInputsTrackingGamePhase == 9)  // What is 9? 
      && ((GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Countdown
        || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.Formation
        || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GridWalk
        || (GTR2GamePhase)scoring.mScoringInfo.mGamePhase == GTR2GamePhase.GreenFlag))
      {
      }

      this.lastMotionAndInputsTrackingGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0
        || extended.mSessionStarted == 0)
      {
        return;
      }

      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!TransitionTracker.idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = TransitionTracker.idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mPlayerTelemetry;

      var opponentInfos = new List<OpponentMotionAndInputsInfo>();
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        var veh = scoring.mVehicles[i];
        var o = new OpponentMotionAndInputsInfo();
        o.mID = veh.mID;
        o.name = TransitionTracker.GetStringFromBytes(veh.mDriverName);
        o.position = veh.mPlace;

        o.vehicleName = TransitionTracker.GetStringFromBytes(veh.mVehicleName);
        o.vehicleClass = TransitionTracker.GetStringFromBytes(veh.mVehicleClass);

        o.worldX = veh.mPos.x;
        o.worldY = veh.mPos.y;
        o.worldZ = veh.mPos.z;

        o.yaw = (float)Math.Atan2(veh.mOriZ.x, veh.mOriZ.z);

        o.pitch = (float)Math.Atan2(-veh.mOriY.z,
          Math.Sqrt(veh.mOriX.z * veh.mOriX.z + veh.mOriZ.z * veh.mOriZ.z));

        o.roll = (float)Math.Atan2(veh.mOriY.x,
          Math.Sqrt(veh.mOriX.x * veh.mOriX.x + veh.mOriZ.x * veh.mOriZ.x));

        o.speedKMH = (float)Math.Sqrt((veh.mLocalVel.x * veh.mLocalVel.x)
          + (veh.mLocalVel.y * veh.mLocalVel.y)
          + (veh.mLocalVel.z * veh.mLocalVel.z)) * 3.6f;

        var ev = extended.mExtendedVehicleScoring[veh.mID % MAX_MAPPED_IDS];
        o.rpm = ev.mEngineRPM;
        o.gear = ev.mGear;
        o.steeringInput = ev.mSteeringInput;
        o.throttleInput = ev.mThrottleInput;
        o.brakeInput = ev.mBrakeInput;
        o.pitState = (GTR2PitState)ev.mPitState;
        o.blueFlag = ev.mBlueFlag;

        var inPits = veh.mInPits != 0;
        if (o.pitState == GTR2PitState.None
          && inPits)
        {
          if (o.speedKMH < 0.5f)
          {
            o.pitState = GTR2PitState.Stopped;
            // Debug.WriteLine($"FAKE STOPPED {o.name}   {scoring.mScoringInfo.mCurrentET}");
          }
          if (o.speedKMH > 5.0f)
          {
            o.pitState = GTR2PitState.Entering;
            //Debug.WriteLine($"FAKE ENTERING {o.name}   {scoring.mScoringInfo.mLapDist}m   {scoring.mScoringInfo.mCurrentET}s");
          }
        }

        opponentInfos.Add(o);
      }

      // Order by pos, ascending.
      opponentInfos.Sort((o1, o2) => o1.position.CompareTo(o2.position));

      if (g != null && MainForm.instance.CurrentPage() == MainForm.Pages.MotionAndInputs)
      {
        var timingsYStart = this.screenYStart + 3.0f;
        var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
        g.DrawString($"{"Name (ID)".PadRight(32)}"
          + $"{"Pos".PadLeft(3)}"
          + $"{"{x, y, z}(m)".PadLeft(37)}"
          + $"{"{yaw, pitch, roll}(rad)".PadLeft(31)}"
          + $"{"Speed(km/h)".PadLeft(14)}"
          + $"{"RPM".PadLeft(11)}"
          + $"{"Gear#".PadLeft(7)}"
          + $"{"Steering".PadLeft(11)}"
          + $"{"Throttle".PadLeft(11)}"
          + $"{"Brake".PadLeft(11)}"
          , font, Brushes.Yellow, 3.0f, 20.0f);

        var currYPos = 35.0f;
        foreach (var oi in opponentInfos)
        {
          this.DrawMotionAndInputsInfo(oi, currYPos, focusVehicleID, g);
          currYPos += 15.0f;
        }

        var focusVeh = new GTR2VehicleScoring();
        var found = false;
        for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
        {
          var veh = scoring.mVehicles[i];
          if (veh.mID == focusVehicleID)
          {
            focusVeh = veh;
            found = true;
            break;
          }
        }

        if (!found)
          focusVeh = scoring.mVehicles[0];

        var ev2 = extended.mExtendedVehicleScoring[focusVeh.mID % MAX_MAPPED_IDS];
        g.DrawString($"Focus Vehicle({focusVehicleID}):", font, Brushes.Yellow, 1500.0f, 20.0f);
        g.DrawString($"{"Steering:".PadRight(20)}{ev2.mSteeringInput.ToString("0.000")}", font, Brushes.Orange, 1500.0f, 35.0f);
        g.DrawString($"{"Throttle:".PadRight(20)}{ev2.mThrottleInput.ToString("0.000")}", font, Brushes.Orange, 1500.0f, 50.0f);
        g.DrawString($"{"Brake:".PadRight(20)}{ev2.mBrakeInput.ToString("0.000")}", font, Brushes.Orange, 1500.0f, 65.0f);
        g.DrawString($"{"Steering  Throttle  Brake"}", font, Brushes.Orange, 1500.0f, 90.0f);

        var p = new Pen(Color.White);
        if (ev2.mSteeringInput < 0.0f)
          g.FillRectangle(Brushes.Lime, 1525.0f, 265.0f, 20.0f, -150.0f * ev2.mSteeringInput);
        else
          g.FillRectangle(Brushes.Lime, 1525.0f, 265.0f - 150.0f * ev2.mSteeringInput, 20.0f, 150.0f * ev2.mSteeringInput);

        g.DrawRectangle(p, 1525.0f, 115.0f, 20.0f, 150.0f);
        g.DrawRectangle(p, 1525.0f, 265.0f, 20.0f, 150.0f);

        g.FillRectangle(Brushes.Blue, 1600.0f, 415.0f - 300.0f * ev2.mThrottleInput, 20.0f, 300.0f * ev2.mThrottleInput);
        g.DrawRectangle(p, 1600.0f, 115.0f, 20.0f, 300.0f);

        g.FillRectangle(Brushes.Red, 1675.0f, 415.0f - 300.0f * ev2.mBrakeInput, 20.0f, 300.0f * ev2.mBrakeInput);
        g.DrawRectangle(p, 1675.0f, 115.0f, 20.0f, 300.0f);

        var otd = PitStopStatesPage.opponentDataMap[focusVeh.mID];
        g.DrawString($"Last Pit Stop:", font, Brushes.Yellow, 1500.0f, 430.0f);
        g.DrawString($"{"Entry Fuel(L):".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? otd.lastPitEntryFuel.ToString("0.0") : "n/a") }", font, Brushes.Orange, 1500.0f, 445.0f);
        g.DrawString($"{"Entry Part Detached:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitEntryAnyPartDetached ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 460.0f);
        g.DrawString($"{"Entry Wheel Dtched:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitEntryAnyWheelDetached ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 475.0f);
        g.DrawString($"{"Entry Wheel Flat:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitEntryAnyWheelFlat ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 490.0f);
        g.DrawString($"{"Entry RWing Dtched:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitEntryRearWingDetached ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 505.0f);
        g.DrawString($"{"Entry Compound:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? otd.lastPitEntryCompound : "n/a") }", font, Brushes.Orange, 1500.0f, 520.0f);

        g.DrawString($"{"Exit Fuel(L):".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? otd.lastPitExitFuel.ToString("0.0") : "n/a") }", font, Brushes.Orange, 1500.0f, 535.0f);
        g.DrawString($"{"Exit Part Detached:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitExitAnyPartDetached ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 550.0f);
        g.DrawString($"{"Exit Wheel Dtched:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitExitAnyWheelDetached ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 565.0f);
        g.DrawString($"{"Exit Wheel Flat:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitExitAnyWheelFlat ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 580.0f);
        g.DrawString($"{"Exit RWing Dtched:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitExitRearWingDetached ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 595.0f);
        g.DrawString($"{"Exit Compound:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? otd.lastPitExitCompound : "n/a") }", font, Brushes.Orange, 1500.0f, 610.0f);
        g.DrawString($"{"Front Tires Chngd:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitEntryFrontWear != otd.lastPitExitFrontWear ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 625.0f);
        g.DrawString($"{"Rear Tires Chngd:".PadRight(20)} { (otd.lastPitStopDuration != 0.0f ? (otd.lastPitEntryRearWear != otd.lastPitExitRearWear ? "yes" : "no") : "n/a") }", font, Brushes.Orange, 1500.0f, 640.0f);
      }
    }

    private void DrawMotionAndInputsInfo(OpponentMotionAndInputsInfo oi, float currYPos, int focusVehId, Graphics g)
    {
      var otd = PitStopStatesPage.opponentDataMap[oi.mID];
      var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
      var brush = Brushes.OrangeRed;
      if (oi.blueFlag != 0)
        brush = Brushes.Blue;
      else if (oi.pitState == GTR2PitState.Entering)
        brush = Brushes.White;
      else if (oi.pitState == GTR2PitState.Request)
        brush = Brushes.Orange;
      else if (oi.pitState == GTR2PitState.Stopped)
        brush = Brushes.Magenta;
      else if (oi.pitState == GTR2PitState.Exiting)
        brush = Brushes.YellowGreen;

      g.DrawString((focusVehId != oi.mID ? $"{$"{oi.name} ({oi.mID})".PadRight(32)}" : $"{$">>> {oi.name} ({oi.mID})".PadRight(32)}")
        + $"{oi.position.ToString().PadLeft(3)}"
        + "{".PadLeft(4) + $"{$"{oi.worldX.ToString("0.0").PadLeft(9)}, {oi.worldY.ToString("0.0").PadLeft(9)}, {oi.worldZ.ToString("0.0").PadLeft(9)}"}" + " }"
        + "{".PadLeft(4) + $"{$"{oi.yaw.ToString("0.000").PadLeft(7)}, {oi.pitch.ToString("0.000").PadLeft(7)}, {oi.roll.ToString("0.000").PadLeft(7)}"}" + " }"
        + $"{oi.speedKMH.ToString("0.00").PadLeft(14)}"
        + $"{oi.rpm.ToString("0.000").PadLeft(11)}"
        + $"{oi.gear.ToString().PadLeft(7)}"
        + $"{(oi.steeringInput).ToString("0.000").PadLeft(11)}"
        + $"{(oi.throttleInput).ToString("0.000").PadLeft(11)}"
        + $"{(oi.brakeInput).ToString("0.000").PadLeft(11)}"
        , font, brush, 3.0f, currYPos);
    }

  }
}
