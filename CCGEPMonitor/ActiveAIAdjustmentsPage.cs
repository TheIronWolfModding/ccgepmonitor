﻿using GTR2SharedMemory.GTR2Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using static GTR2SharedMemory.GTR2Constants;

namespace CCGEPMonitor
{
  class ActiveAIAdjustmentsPage
  {
    internal class ActiveAIAdjustments
    {
      internal string name = null;
      internal int position = -1;
      internal float activeTireWearAdjustmentPct = 0.0f;
      internal float activeFuelUseAdjustmentPct = 0.0f;
      internal GTR2AITireChangeDelayReason tireChangeDelayReason = GTR2AITireChangeDelayReason.NoDelay;
      internal GTR2PitState pitState = GTR2PitState.None;
      internal int blueFlag = 0;
      internal float speedKMH = 0.0f;

      internal long mID = -1;
    }

    private GTR2GamePhase lastGamePhase = GTR2GamePhase.SessionOver;
    private float screenYStart = 293.0f;

    internal void RenderPage(ref GTR2Scoring scoring, ref GTR2Telemetry telemetry, ref GTR2Extended extended, int focusVehId, Graphics g)
    {
      this.lastGamePhase = (GTR2GamePhase)scoring.mScoringInfo.mGamePhase;

      if (scoring.mScoringInfo.mNumVehicles == 0
        || extended.mSessionStarted == 0)
      {
        return;
      }

      if (TransitionTracker.playerVeh.mIsPlayer != 1)
        return;

      var scoringPlrId = TransitionTracker.playerVeh.mID;
      if (!TransitionTracker.idsToTelIndices.ContainsKey(scoringPlrId))
        return;

      var resolvedIdx = TransitionTracker.idsToTelIndices[scoringPlrId];
      var playerVehTelemetry = telemetry.mPlayerTelemetry;

      var opponentInfos = new List<ActiveAIAdjustments>();
      for (int i = 0; i < scoring.mScoringInfo.mNumVehicles; ++i)
      {
        var veh = scoring.mVehicles[i];
        var o = new ActiveAIAdjustments();
        o.mID = veh.mID;
        o.name = TransitionTracker.GetStringFromBytes(veh.mDriverName);
        o.position = veh.mPlace;

        var ev = extended.mExtendedVehicleScoring[veh.mID % MAX_MAPPED_IDS];
        o.activeTireWearAdjustmentPct = ev.mActiveTireWearAdjustmentPct;
        o.activeFuelUseAdjustmentPct = ev.mActiveFuelUseAdjustmentPct;
        o.tireChangeDelayReason = (GTR2AITireChangeDelayReason)ev.mAITireChangeDelayReason;
        o.pitState = (GTR2PitState)ev.mPitState;
        o.blueFlag = ev.mBlueFlag;

        var inPits = veh.mInPits != 0;
        if (o.pitState == GTR2PitState.None
          && inPits)
        {
          if (o.speedKMH < 0.5f)
          {
            o.pitState = GTR2PitState.Stopped;
            // Debug.WriteLine($"FAKE STOPPED {o.name}   {scoring.mScoringInfo.mCurrentET}");
          }
          if (o.speedKMH > 5.0f)
          {
            o.pitState = GTR2PitState.Entering;
            //Debug.WriteLine($"FAKE ENTERING {o.name}   {scoring.mScoringInfo.mLapDist}m   {scoring.mScoringInfo.mCurrentET}s");
          }
        }
        opponentInfos.Add(o);
      }

      // Order by pos, ascending.
      opponentInfos.Sort((o1, o2) => o1.position.CompareTo(o2.position));

      if (g != null && MainForm.instance.CurrentPage() == MainForm.Pages.AIAdjustments)
      {
        var timingsYStart = this.screenYStart + 3.0f;
        var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
        g.DrawString($"{"Name (ID)".PadRight(29)}"
          + $"{"TW".PadLeft(7)}"
          + $"{"FU".PadLeft(7)}"
          + $"{"TCDR".PadLeft(27)}"
          , font, Brushes.Yellow, 3.0f, 20.0f);

        var currYPos = 35.0f;
        foreach (var oi in opponentInfos)
        {
          this.RenderOpponentInfo(oi, currYPos, focusVehId, g);
          currYPos += 15.0f;
        }

        g.DrawString("Label:", font, Brushes.Yellow, 1665.0f, 20.0f);
        g.DrawString("TW - Tire Wear adj. %", font, Brushes.Orange, 1665.0f, 35.0f);
        g.DrawString("FU - Fuel Use adj. %", font, Brushes.Orange, 1665.0f, 50.0f);
        g.DrawString("TCDR - Tire Change", font, Brushes.Orange, 1665.0f, 65.0f);
        g.DrawString("       Delay Reason", font, Brushes.Orange, 1665.0f, 75.0f);
      }
    }

    private void RenderOpponentInfo(ActiveAIAdjustments oi, float currYPos, int focusVehId, Graphics g)
    {
      var otd = PitStopStatesPage.opponentDataMap[oi.mID];
      var font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Bold);
      var brush = Brushes.OrangeRed;
      if (oi.blueFlag != 0)
        brush = Brushes.Blue;
      else if (oi.pitState == GTR2PitState.Entering)
        brush = Brushes.White;
      else if (oi.pitState == GTR2PitState.Request)
        brush = Brushes.Orange;
      else if (oi.pitState == GTR2PitState.Stopped)
        brush = Brushes.Magenta;
      else if (oi.pitState == GTR2PitState.Exiting)
        brush = Brushes.YellowGreen;

      g.DrawString((focusVehId != oi.mID ? $"{$"{oi.name} ({oi.mID})".PadRight(29)}" : $"{$">>> {oi.name} ({oi.mID})".PadRight(29)}")
        + $"{oi.activeTireWearAdjustmentPct.ToString("0.0")}%".PadLeft(7)
        + $"{oi.activeFuelUseAdjustmentPct.ToString("0.0")}%".PadLeft(7)
        + $"{oi.tireChangeDelayReason.ToString()}".PadLeft(27)
        , font, brush, 3.0f, currYPos);
    }
  }
}
