﻿/*
MainForm Designer part of file.

Author: The Iron Wolf
Website: thecrewchief.org
*/
using System.Windows.Forms;

namespace CCGEPMonitor
{
		partial class MainForm
		{
				/// <summary>
				/// Required designer variable.
				/// </summary>
				private System.ComponentModel.IContainer components = null;

				#region Windows Form Designer generated code

				/// <summary>
				/// Required method for Designer support - do not modify
				/// the contents of this method with the code editor.
				/// </summary>
				private void InitializeComponent()
				{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.view = new System.Windows.Forms.PictureBox();
			this.scaleLabel = new System.Windows.Forms.Label();
			this.scaleTextBox = new System.Windows.Forms.TextBox();
			this.focusVehLabel = new System.Windows.Forms.Label();
			this.focusVehTextBox = new System.Windows.Forms.TextBox();
			this.setAsOriginCheckBox = new System.Windows.Forms.CheckBox();
			this.groupBoxFocus = new System.Windows.Forms.GroupBox();
			this.rotateAroundCheckBox = new System.Windows.Forms.CheckBox();
			this.globalGroupBox = new System.Windows.Forms.GroupBox();
			this.yOffsetTextBox = new System.Windows.Forms.TextBox();
			this.yOffsetLabel = new System.Windows.Forms.Label();
			this.xOffsetTextBox = new System.Windows.Forms.TextBox();
			this.xOffsetLabel = new System.Windows.Forms.Label();
			this.loggingGroupBox = new System.Windows.Forms.GroupBox();
			this.logRulesCheckBox = new System.Windows.Forms.CheckBox();
			this.logTimingCheckBox = new System.Windows.Forms.CheckBox();
			this.logDamageCheckBox = new System.Windows.Forms.CheckBox();
			this.logPhaseAndStateCheckBox = new System.Windows.Forms.CheckBox();
			this.lightModeCheckBox = new System.Windows.Forms.CheckBox();
			this.inputsGroupBox = new System.Windows.Forms.GroupBox();
			this.enablePitInputsCheckBox = new System.Windows.Forms.CheckBox();
			this.rainIntensityLabel = new System.Windows.Forms.Label();
			this.rainIntensityTextBox = new System.Windows.Forms.TextBox();
			this.applyRainIntensityButton = new System.Windows.Forms.Button();
			this.enablePitInputsToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.pageComboBox = new System.Windows.Forms.ComboBox();
			this.labelPage = new System.Windows.Forms.Label();
			this.darkThemeCheckBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.view)).BeginInit();
			this.groupBoxFocus.SuspendLayout();
			this.globalGroupBox.SuspendLayout();
			this.loggingGroupBox.SuspendLayout();
			this.inputsGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// view
			// 
			this.view.BackColor = System.Drawing.Color.Black;
			this.view.Location = new System.Drawing.Point(0, 66);
			this.view.Margin = new System.Windows.Forms.Padding(0);
			this.view.Name = "view";
			this.view.Size = new System.Drawing.Size(1899, 1034);
			this.view.TabIndex = 0;
			this.view.TabStop = false;
			// 
			// scaleLabel
			// 
			this.scaleLabel.AutoSize = true;
			this.scaleLabel.Location = new System.Drawing.Point(8, 18);
			this.scaleLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.scaleLabel.Name = "scaleLabel";
			this.scaleLabel.Size = new System.Drawing.Size(47, 17);
			this.scaleLabel.TabIndex = 1;
			this.scaleLabel.Text = "Scale:";
			// 
			// scaleTextBox
			// 
			this.scaleTextBox.AcceptsReturn = true;
			this.scaleTextBox.Location = new System.Drawing.Point(65, 13);
			this.scaleTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.scaleTextBox.Name = "scaleTextBox";
			this.scaleTextBox.Size = new System.Drawing.Size(80, 22);
			this.scaleTextBox.TabIndex = 2;
			// 
			// focusVehLabel
			// 
			//this.focusVehLabel.AutoSize = true;
			var focuVehToolTipText = "Each Vehicle has an ID, number in the parenthesis.\nYou can use ID to get more detailed info for Vehicle of interest.";
			this.focusVehLabel.Location = new System.Drawing.Point(9, 22);
			this.focusVehLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.focusVehLabel.Name = "focusVehLabel";
			this.focusVehLabel.Size = new System.Drawing.Size(100, 17);
			this.focusVehLabel.TabIndex = 2;
			this.focusVehLabel.Text = "&Focus ID:";
			this.focusVehLabelToolTip = new ToolTip();
			this.focusVehLabelToolTip.ShowAlways = false;
			this.focusVehLabelToolTip.SetToolTip(this.focusVehLabel, focuVehToolTipText);
			// 
			// focusVehTextBox
			// 
			this.focusVehTextBox.Location = new System.Drawing.Point(89, 20);
			this.focusVehTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.focusVehTextBox.Name = "focusVehTextBox";
			this.focusVehTextBox.Size = new System.Drawing.Size(71, 22);
			this.focusVehTextBox.TabIndex = 3;
			this.focusVehTextBoxToolTip = new ToolTip();
			this.focusVehTextBoxToolTip.ShowAlways = false;
			this.focusVehTextBoxToolTip.SetToolTip(this.focusVehTextBox, focuVehToolTipText);

			// 
			// setAsOriginCheckBox
			// 
			this.setAsOriginCheckBox.AutoSize = true;
			this.setAsOriginCheckBox.Location = new System.Drawing.Point(172, 16);
			this.setAsOriginCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.setAsOriginCheckBox.Name = "setAsOriginCheckBox";
			this.setAsOriginCheckBox.Size = new System.Drawing.Size(112, 21);
			this.setAsOriginCheckBox.TabIndex = 6;
			this.setAsOriginCheckBox.Text = "Set as Origin";
			this.setAsOriginCheckBox.UseVisualStyleBackColor = true;
			// 
			// groupBoxFocus
			// 
			this.groupBoxFocus.Controls.Add(this.rotateAroundCheckBox);
			this.groupBoxFocus.Controls.Add(this.focusVehTextBox);
			this.groupBoxFocus.Controls.Add(this.setAsOriginCheckBox);
			this.groupBoxFocus.Controls.Add(this.focusVehLabel);
			this.groupBoxFocus.Location = new System.Drawing.Point(577, -1);
			this.groupBoxFocus.Margin = new System.Windows.Forms.Padding(4);
			this.groupBoxFocus.Name = "groupBoxFocus";
			this.groupBoxFocus.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxFocus.Size = new System.Drawing.Size(353, 66);
			this.groupBoxFocus.TabIndex = 7;
			this.groupBoxFocus.TabStop = false;
			this.groupBoxFocus.Text = "Focus";
			// 
			// rotateAroundCheckBox
			// 
			this.rotateAroundCheckBox.AutoSize = true;
			this.rotateAroundCheckBox.Location = new System.Drawing.Point(172, 39);
			this.rotateAroundCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.rotateAroundCheckBox.Name = "rotateAroundCheckBox";
			this.rotateAroundCheckBox.Size = new System.Drawing.Size(169, 21);
			this.rotateAroundCheckBox.TabIndex = 8;
			this.rotateAroundCheckBox.Text = "Set as Rotation Origin";
			this.rotateAroundCheckBox.UseVisualStyleBackColor = true;
			// 
			// globalGroupBox
			// 
			this.globalGroupBox.Controls.Add(this.yOffsetTextBox);
			this.globalGroupBox.Controls.Add(this.yOffsetLabel);
			this.globalGroupBox.Controls.Add(this.xOffsetTextBox);
			this.globalGroupBox.Controls.Add(this.xOffsetLabel);
			this.globalGroupBox.Controls.Add(this.scaleTextBox);
			this.globalGroupBox.Controls.Add(this.scaleLabel);
			this.globalGroupBox.Location = new System.Drawing.Point(297, -1);
			this.globalGroupBox.Margin = new System.Windows.Forms.Padding(4);
			this.globalGroupBox.Name = "globalGroupBox";
			this.globalGroupBox.Padding = new System.Windows.Forms.Padding(4);
			this.globalGroupBox.Size = new System.Drawing.Size(272, 66);
			this.globalGroupBox.TabIndex = 8;
			this.globalGroupBox.TabStop = false;
			this.globalGroupBox.Text = "Global";
			// 
			// yOffsetTextBox
			// 
			this.yOffsetTextBox.Location = new System.Drawing.Point(201, 40);
			this.yOffsetTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.yOffsetTextBox.Name = "yOffsetTextBox";
			this.yOffsetTextBox.Size = new System.Drawing.Size(67, 22);
			this.yOffsetTextBox.TabIndex = 6;
			// 
			// yOffsetLabel
			// 
			this.yOffsetLabel.AutoSize = true;
			this.yOffsetLabel.Location = new System.Drawing.Point(149, 44);
			this.yOffsetLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.yOffsetLabel.Name = "yOffsetLabel";
			this.yOffsetLabel.Size = new System.Drawing.Size(58, 17);
			this.yOffsetLabel.TabIndex = 5;
			this.yOffsetLabel.Text = "y offset:";
			// 
			// xOffsetTextBox
			// 
			this.xOffsetTextBox.Location = new System.Drawing.Point(201, 12);
			this.xOffsetTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.xOffsetTextBox.Name = "xOffsetTextBox";
			this.xOffsetTextBox.Size = new System.Drawing.Size(67, 22);
			this.xOffsetTextBox.TabIndex = 4;
			// 
			// xOffsetLabel
			// 
			this.xOffsetLabel.AutoSize = true;
			this.xOffsetLabel.Location = new System.Drawing.Point(150, 17);
			this.xOffsetLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.xOffsetLabel.Name = "xOffsetLabel";
			this.xOffsetLabel.Size = new System.Drawing.Size(57, 17);
			this.xOffsetLabel.TabIndex = 3;
			this.xOffsetLabel.Text = "x offset:";
			// 
			// loggingGroupBox
			// 
			this.loggingGroupBox.Controls.Add(this.logRulesCheckBox);
			this.loggingGroupBox.Controls.Add(this.logTimingCheckBox);
			this.loggingGroupBox.Controls.Add(this.logDamageCheckBox);
			this.loggingGroupBox.Controls.Add(this.logPhaseAndStateCheckBox);
			this.loggingGroupBox.Location = new System.Drawing.Point(941, -1);
			this.loggingGroupBox.Margin = new System.Windows.Forms.Padding(4);
			this.loggingGroupBox.Name = "loggingGroupBox";
			this.loggingGroupBox.Padding = new System.Windows.Forms.Padding(4);
			this.loggingGroupBox.Size = new System.Drawing.Size(341, 66);
			this.loggingGroupBox.TabIndex = 9;
			this.loggingGroupBox.TabStop = false;
			this.loggingGroupBox.Text = "File Logging";
			// 
			// logRulesCheckBox
			// 
			this.logRulesCheckBox.AutoSize = true;
			this.logRulesCheckBox.Enabled = false;
			this.logRulesCheckBox.Location = new System.Drawing.Point(145, 39);
			this.logRulesCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.logRulesCheckBox.Name = "logRulesCheckBox";
			this.logRulesCheckBox.Size = new System.Drawing.Size(66, 21);
			this.logRulesCheckBox.TabIndex = 12;
			this.logRulesCheckBox.Text = "Rules";
			this.logRulesCheckBox.UseVisualStyleBackColor = true;
			// 
			// logTimingCheckBox
			// 
			this.logTimingCheckBox.AutoSize = true;
			this.logTimingCheckBox.Location = new System.Drawing.Point(145, 16);
			this.logTimingCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.logTimingCheckBox.Name = "logTimingCheckBox";
			this.logTimingCheckBox.Size = new System.Drawing.Size(72, 21);
			this.logTimingCheckBox.TabIndex = 11;
			this.logTimingCheckBox.Text = "Timing";
			this.logTimingCheckBox.UseVisualStyleBackColor = true;
			// 
			// logDamageCheckBox
			// 
			this.logDamageCheckBox.AutoSize = true;
			this.logDamageCheckBox.Location = new System.Drawing.Point(9, 39);
			this.logDamageCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.logDamageCheckBox.Name = "logDamageCheckBox";
			this.logDamageCheckBox.Size = new System.Drawing.Size(83, 21);
			this.logDamageCheckBox.TabIndex = 10;
			this.logDamageCheckBox.Text = "Damage";
			this.logDamageCheckBox.UseVisualStyleBackColor = true;
			// 
			// logPhaseAndStateCheckBox
			// 
			this.logPhaseAndStateCheckBox.AutoSize = true;
			this.logPhaseAndStateCheckBox.Location = new System.Drawing.Point(9, 16);
			this.logPhaseAndStateCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.logPhaseAndStateCheckBox.Name = "logPhaseAndStateCheckBox";
			this.logPhaseAndStateCheckBox.Size = new System.Drawing.Size(135, 21);
			this.logPhaseAndStateCheckBox.TabIndex = 9;
			this.logPhaseAndStateCheckBox.Text = "Phase and State";
			this.logPhaseAndStateCheckBox.UseVisualStyleBackColor = true;
			// 
			// lightModeCheckBox
			// 
			this.lightModeCheckBox.AutoSize = true;
			this.lightModeCheckBox.Location = new System.Drawing.Point(7, 45);
			this.lightModeCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.lightModeCheckBox.Name = "lightModeCheckBox";
			this.lightModeCheckBox.Size = new System.Drawing.Size(100, 21);
			this.lightModeCheckBox.TabIndex = 11;
			this.lightModeCheckBox.Text = "Reduced CPU Usage Mode";
			this.lightModeCheckBox.UseVisualStyleBackColor = true;
			this.lightModeCheckBoxToolTip = new ToolTip();
			this.lightModeCheckBoxToolTip.ShowAlways = false;
			this.lightModeCheckBoxToolTip.SetToolTip(this.lightModeCheckBox, "If checked, Monitor will update less frequently, thus reducing the CPU usage.\n\nThis is useful for reducing the FPS impact if you are playing the game and run\nMonitor in the background to collect some stats that you plan to check after the race.");
			// 
			// inputsGroupBox
			// 
			this.inputsGroupBox.Controls.Add(this.enablePitInputsCheckBox);
			this.inputsGroupBox.Controls.Add(this.rainIntensityLabel);
			this.inputsGroupBox.Controls.Add(this.rainIntensityTextBox);
			this.inputsGroupBox.Controls.Add(this.applyRainIntensityButton);
			this.inputsGroupBox.Location = new System.Drawing.Point(1296, -1);
			this.inputsGroupBox.Margin = new System.Windows.Forms.Padding(4);
			this.inputsGroupBox.Name = "inputsGroupBox";
			this.inputsGroupBox.Padding = new System.Windows.Forms.Padding(4);
			this.inputsGroupBox.Size = new System.Drawing.Size(341, 66);
			this.inputsGroupBox.TabIndex = 15;
			this.inputsGroupBox.TabStop = false;
			this.inputsGroupBox.Text = "Inputs";
			// 
			// enablePitInputsCheckBox
			// 
			this.enablePitInputsCheckBox.AutoSize = true;
			this.enablePitInputsCheckBox.Location = new System.Drawing.Point(9, 16);
			this.enablePitInputsCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.enablePitInputsCheckBox.Name = "enablePitInputsCheckBox";
			this.enablePitInputsCheckBox.Size = new System.Drawing.Size(136, 21);
			this.enablePitInputsCheckBox.TabIndex = 20;
			this.enablePitInputsCheckBox.Text = "Enable Pit Inputs";
			this.enablePitInputsToolTip.SetToolTip(this.enablePitInputsCheckBox, "Control GTR2 Pit menu using Y, U, O and P keys.  Note that Pit Menu buffer and En" +
				"ableHWControlInput should be enabled.");
			this.enablePitInputsCheckBox.UseVisualStyleBackColor = true;
			// 
			// rainIntensityLabel
			// 
			this.rainIntensityLabel.AutoSize = true;
			this.rainIntensityLabel.Location = new System.Drawing.Point(7, 39);
			this.rainIntensityLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.rainIntensityLabel.Name = "rainIntensityLabel";
			this.rainIntensityLabel.Size = new System.Drawing.Size(97, 17);
			this.rainIntensityLabel.TabIndex = 25;
			this.rainIntensityLabel.Text = "Rain intensity:";
			// 
			// rainIntensityTextBox
			// 
			this.rainIntensityTextBox.Location = new System.Drawing.Point(107, 37);
			this.rainIntensityTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.rainIntensityTextBox.Name = "rainIntensityTextBox";
			this.rainIntensityTextBox.Size = new System.Drawing.Size(52, 22);
			this.rainIntensityTextBox.TabIndex = 30;
			// 
			// applyRainIntensityButton
			// 
			this.applyRainIntensityButton.Enabled = false;
			this.applyRainIntensityButton.Location = new System.Drawing.Point(167, 36);
			this.applyRainIntensityButton.Margin = new System.Windows.Forms.Padding(4);
			this.applyRainIntensityButton.Name = "applyRainIntensityButton";
			this.applyRainIntensityButton.Size = new System.Drawing.Size(93, 27);
			this.applyRainIntensityButton.TabIndex = 35;
			this.applyRainIntensityButton.Text = "Apply";
			// 
			// pageComboBox
			// 
			this.pageComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.pageComboBox.FormattingEnabled = true;
			this.pageComboBox.Items.AddRange(new object[] {
						"Pit Stop Analysis",
						"Active AI Adjustments",
						"Motion, Inputs & Focused View",
						"Player Vehicle, Damage & Timings"});
			this.pageComboBox.Location = new System.Drawing.Point(57, 12);
			this.pageComboBox.Name = "pageComboBox";
			this.pageComboBox.Size = new System.Drawing.Size(233, 24);
			this.pageComboBox.TabIndex = 1;
			// 
			// labelPage
			// 
			this.labelPage.AutoSize = true;
			this.labelPage.Location = new System.Drawing.Point(7, 15);
			this.labelPage.Name = "labelPage";
			this.labelPage.Size = new System.Drawing.Size(45, 17);
			this.labelPage.TabIndex = 1;
			this.labelPage.Text = "&Page:";
			// 
			// darkThemCeheckBox
			// 
			this.darkThemeCheckBox.AutoSize = true;
			this.darkThemeCheckBox.Checked = true;
			this.darkThemeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.darkThemeCheckBox.Location = new System.Drawing.Point(180, 45);
			this.darkThemeCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.darkThemeCheckBox.Name = "darkThemCeheckBox";
			this.darkThemeCheckBox.Size = new System.Drawing.Size(103, 21);
			this.darkThemeCheckBox.TabIndex = 18;
			this.darkThemeCheckBox.Text = "Dark Theme";
			this.darkThemeCheckBox.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(1898, 1029);
			this.Controls.Add(this.darkThemeCheckBox);
			this.Controls.Add(this.labelPage);
			this.Controls.Add(this.pageComboBox);
			this.Controls.Add(this.lightModeCheckBox);
			this.Controls.Add(this.loggingGroupBox);
			this.Controls.Add(this.globalGroupBox);
			this.Controls.Add(this.groupBoxFocus);
			this.Controls.Add(this.inputsGroupBox);
			this.Controls.Add(this.view);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "Crew Chief GTR2 Enhancements Plugin Monitor";
			((System.ComponentModel.ISupportInitialize)(this.view)).EndInit();
			this.groupBoxFocus.ResumeLayout(false);
			this.groupBoxFocus.PerformLayout();
			this.globalGroupBox.ResumeLayout(false);
			this.globalGroupBox.PerformLayout();
			this.loggingGroupBox.ResumeLayout(false);
			this.loggingGroupBox.PerformLayout();
			this.inputsGroupBox.ResumeLayout(false);
			this.inputsGroupBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

				}

		#endregion

		private System.Windows.Forms.PictureBox view;
		private System.Windows.Forms.Label scaleLabel;
		private System.Windows.Forms.TextBox scaleTextBox;
		private System.Windows.Forms.Label focusVehLabel;
		private System.Windows.Forms.TextBox focusVehTextBox;
		private System.Windows.Forms.CheckBox setAsOriginCheckBox;
		private System.Windows.Forms.GroupBox groupBoxFocus;
		private System.Windows.Forms.CheckBox rotateAroundCheckBox
;
		private System.Windows.Forms.GroupBox globalGroupBox;
		private System.Windows.Forms.TextBox xOffsetTextBox;
		private System.Windows.Forms.Label xOffsetLabel;
		private System.Windows.Forms.TextBox yOffsetTextBox;
		private System.Windows.Forms.Label yOffsetLabel;
		private System.Windows.Forms.GroupBox loggingGroupBox;
		private System.Windows.Forms.CheckBox logPhaseAndStateCheckBox;
		private System.Windows.Forms.CheckBox lightModeCheckBox;
		private System.Windows.Forms.CheckBox logDamageCheckBox;
		private System.Windows.Forms.CheckBox logTimingCheckBox;
		private System.Windows.Forms.CheckBox logRulesCheckBox;
		private System.Windows.Forms.GroupBox inputsGroupBox;
		private System.Windows.Forms.CheckBox enablePitInputsCheckBox;
		private System.Windows.Forms.ToolTip enablePitInputsToolTip;
		private System.Windows.Forms.Label rainIntensityLabel;
		private System.Windows.Forms.TextBox rainIntensityTextBox;
		private System.Windows.Forms.Button applyRainIntensityButton;
		private System.Windows.Forms.ComboBox pageComboBox;
		private System.Windows.Forms.Label labelPage;
		private System.Windows.Forms.CheckBox darkThemeCheckBox;
		private ToolTip lightModeCheckBoxToolTip;
		private ToolTip focusVehTextBoxToolTip;
		private ToolTip focusVehLabelToolTip;
	}
}

